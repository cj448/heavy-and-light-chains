#!/bin/bash

# set max wallclock time
#SBATCH --time=99:00:00

# set the number of nodes
#SBATCH --nodes=1

# memory requirement
#SBATCH --mem=150g

# CPUs allocated to each task
#SBATCH --cpus-per-task=36

# mail alert at start, end and abortion of execution
#SBATCH --mail-type=BEGIN,END,FAIL --mail-user=cole.jensen@yale.edu

#SBATCH -p pi_kleinstein

module load R/4.1.0-foss-2020b

mutational_rate=$1
lc_model=$2
cut_off=$3
nprocs=$4
niter=$5
proportion_light=$6
bootstrap_amount=$7
quiet=$8
build=$9

Rscript sim_full.R $mutational_rate $lc_model $cut_off $nprocs $niter $proportion_light $bootstrap_amount $quiet $build