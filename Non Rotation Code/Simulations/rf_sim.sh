#!/bin/bash

# set max wallclock time
#SBATCH --time=99:00:00

# set the number of nodes
#SBATCH --nodes=1

# memory requirement
#SBATCH --mem=70g

# CPUs allocated to each task
#SBATCH --cpus-per-task=20

# mail alert at start, end and abortion of execution
#SBATCH --mail-type=BEGIN,END,FAIL --mail-user=cole.jensen@yale.edu

#SBATCH -p pi_kleinstein

module load R/4.1.0-foss-2020b

mutational_rate=$1
lc_model=$2
cut_off=$3
nprocs=$4
niter=$5

Rscript robinson_fold_sim.R $mutational_rate $lc_model $cut_off $nprocs $niter