#!/bin/bash

#SBATCH -p pi_kleinstein

sbatch sim_bootstrapping.sh 0.25 36 0 25
sbatch sim_bootstrapping.sh 0.25 36 0.25 25
sbatch sim_bootstrapping.sh 0.25 36 0.5 25
sbatch sim_bootstrapping.sh 0.25 36 0.75 25
sbatch sim_bootstrapping.sh 0.25 36 1 25
sbatch sim_bootstrapping.sh 0.5 36 0 25
sbatch sim_bootstrapping.sh 0.5 36 0.5 25
sbatch sim_bootstrapping.sh 0.5 36 0.5 25
sbatch sim_bootstrapping.sh 0.5 36 0.75 25
sbatch sim_bootstrapping.sh 0.5 36 1 25
sbatch sim_bootstrapping.sh 0.75 36 0 25
sbatch sim_bootstrapping.sh 0.75 36 0.75 25
sbatch sim_bootstrapping.sh 0.75 36 0.75 25
sbatch sim_bootstrapping.sh 0.75 36 0.75 25
sbatch sim_bootstrapping.sh 0.75 36 1 25
sbatch sim_bootstrapping.sh 1 36 0 25
sbatch sim_bootstrapping.sh 1 36 1 25
sbatch sim_bootstrapping.sh 1 36 1 25
sbatch sim_bootstrapping.sh 1 36 1 25
sbatch sim_bootstrapping.sh 1 36 1 25