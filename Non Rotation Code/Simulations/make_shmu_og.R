####### Script to make og_bcr-phylo data with trees

bcr_phylo <- "250_sel"
# change this later
hclones <- readRDS(paste0("~/Documents/Simulations/bcr-phylo/starting_data/hclones_250_50_sel.rds"))

lineage_tree <- read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_0.nw"))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_1.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_2.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_3.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_4.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_5.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_6.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_7.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_8.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_9.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_10.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_11.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_12.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_13.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_14.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_15.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_16.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_17.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_18.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_19.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_20.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_21.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_22.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_23.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_24.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_25.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_26.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_27.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_28.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_29.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_30.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_31.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_32.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_33.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_34.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_35.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_36.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_37.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_38.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_39.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_40.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_41.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_42.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_43.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_44.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_45.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_46.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_47.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_48.nw")))
lineage_tree <- append(lineage_tree, read.tree(file = paste0("~/Documents/Simulations/bcr-phylo/data/unpickled_trees/", bcr_phylo, "/starting_tree_49.nw")))


hclones$clone_id <- gsub(".{2}$", "", hclones$clone_id)
hclones$clone_id <- as.numeric(hclones$clone_id)
hclones <- hclones[order(hclones$clone_id),]

hclones$trees <- NA
for(i in 1:nrow(hclones)){
  # find the sequence IDs
  sub <- filter(hclones, clone_id == i)
  seqs <- gsub("\\_.*", "", sub$data[[1]]@data$sequence_id)
  linage_tips <- substring(lineage_tree[[i]]$tip.label, 9)
  seqs_to_drop <- setdiff(linage_tips, seqs)
  # add back in the starting info 
  if(length(seqs_to_drop > 0)){
    starting_code <- substr(lineage_tree[[i]]$tip.label[1], 1, 8)
    seqs_to_drop <- paste0(starting_code, seqs_to_drop)
    lineage_sub <- drop.tip(lineage_tree[[i]], seqs_to_drop)
    hclones$trees[i] <- list(lineage_sub)
  } else {
    hclones$trees[i] <- list(lineage_tree[[i]])
  }
}

# now save it 
saveRDS(hclones, "~/Documents/Simulations/bcr-phylo/starting_data/og_250_50_sel.rds")

