bs_dnaml <- read.csv("~/Downloads/avg_df_dnaml_flu .csv")
bs_dnapars <- read.csv("~/Downloads/avg_df_dnapars _ flu .csv")
bs_pratchet <- read.csv("~/Downloads/avg_df_pratchet _ flu .csv")
bs_pml <- read.csv("~/Downloads/avg_df_pml _ flu .csv")

bs_all <- rbind(bs_dnaml,bs_dnapars, bs_pml, bs_pratchet)

ggplot(bs_all, aes(x=prop, y=values))+
  theme_bw() +
  geom_line(aes(color = build), size = 2) +
  geom_point(aes(color=build), size = 7) +
  scale_color_manual(values=c("#002B5C", "#00471B", "#F9A01B", "#3E2680")) +
  ylab("Bootstrap Score") +
  xlab("Amount of Light Chain Included")


#test <- readRDS("~/Downloads/results_0.1_pratchet_flu.rds")


rf_dnaml <- read.csv("~/Downloads/all_builds_ dnaml _ flu .csv")
rf_dnapars <- read.csv("~/Downloads/all_builds_ dnapars _ flu .csv")
rf_pratchet <- read.csv("~/Downloads/all_builds_ pratchet _ flu .csv")
rf_pml <- read.csv("~/Downloads/all_builds_ pml _ flu .csv")

rf_all <- rbind(rf_dnaml,rf_dnapars, rf_pml, rf_pratchet)

rf <- ggplot(rf_all, aes(x=props, y=distances))+
  theme_bw() +
  geom_line(aes(color = build), size = 2) +
  geom_point(aes(color=build), size = 7) +
  scale_color_manual(values=c("#002B5C", "#00471B", "#F9A01B", "#3E2680")) +
  ylab("RF Distance") +
  xlab("Amount of Light Chain Included")
rf


#### by seq bin
results_pml <- readRDS("~/Downloads/results_0_pml_flu.rds")
results_pratchet <- readRDS("~/Downloads/results_0_pratchet_flu.rds")
results_dnaml <- readRDS("~/Downloads/results_0_dnaml_flu.rds")
results_dnapars <- readRDS("~/Downloads/results_0_dnapars_flu.rds")
histinfo <- hist(results_pratchet$seqs, breaks = 3)

smallest_rf_pml <- filter(results_pml, seqs <= histinfo$breaks[2])
smaller_rf_pml <- filter(results_pml, seqs <= histinfo$breaks[3] & seqs > histinfo$breaks[2])
mid_rf_pml <- filter(results_pml, seqs <= histinfo$breaks[4] & seqs > histinfo$breaks[3])
big_rf_pml <- filter(results_pml, seqs <= histinfo$breaks[5] & seqs > histinfo$breaks[4])



ggplot(rf_all, aes(x=props, y=distances))+
  theme_bw() +
  geom_line(aes(color = build), size = 2) +
  geom_point(aes(color=build), size = 7) +
  scale_color_manual(values=c("#002B5C", "#00471B", "#F9A01B", "#3E2680")) +
  ylab("RF Distance") +
  xlab("Amount of Light Chain Included")


rf_norm <- function(mask_amount, build, data_source, status){
  # read in the data
  #comp <- read.csv(paste0("~/Downloads/comp_", mask_amount, "_", build, "_", data_source,".csv"))
  rf <- readRDS(paste0("~/Downloads/boxplots_data_", mask_amount, "_", build, "_", data_source, ".rds"))
  #  figure out data to play with 
  if(status == "Heavy"){
    rf_normalized <- mean(filter(rf, status == "Heavy")$distance/ filter(rf, status == "Heavy")$seqs)
  } else if(status == "Heavy and Light"){
    rf_normalized <- mean(filter(rf, status == "Heavy and Light")$distance/ filter(rf, status == "Heavy and Light")$seqs)
  }
  return(rf_normalized)
}

bs_norm <- function(mask_amount, build, data_source, status){
  # read in the data
  comp <- read.csv(paste0("~/Downloads/comp_", mask_amount, "_", build, "_", data_source,".csv"))
  #rf <- readRDS(paste0("~/Downloads/boxplots_data_", mask_amount, "_", build, "_", data_source, ".rds"))
  #  figure out data to play with 
  if(status == "Heavy"){
    bs_normalized <- mean(comp$hclones_avg/comp$seq_size)
  } else if(status == "Heavy and Light"){
    bs_normalized <- mean(comp$hlclones_avg/comp$seq_size)
  }
  return(bs_normalized)
}

build_df <- function(build, data_source){
  distances <- c(rf_norm(0, build, data_source, "Heavy"), rf_norm(0.95, build, data_source, "Heavy and Light"), rf_norm(0.9, build, data_source, "Heavy and Light"),
                 rf_norm(0.85, build, data_source, "Heavy and Light"), rf_norm(0.8, build, data_source, "Heavy and Light"), rf_norm(0.7, build, data_source, "Heavy and Light"),
                 rf_norm(0.6, build, data_source, "Heavy and Light"), rf_norm(0.5, build, data_source, "Heavy and Light"), rf_norm(0.4, build, data_source, "Heavy and Light"),
                 rf_norm(0.3, build, data_source, "Heavy and Light"), rf_norm(0.2, build, data_source, "Heavy and Light"), rf_norm(0.15, build, data_source, "Heavy and Light"),
                 rf_norm(0.1, build, data_source, "Heavy and Light"), rf_norm(0.05, build, data_source, "Heavy and Light"), rf_norm(0, build, data_source, "Heavy and Light")) 
  props <- c(0,5,10,15,20,30,40,50,60,70,80,85,90,95,100)
  rf_all <- data.frame(props=props, distances=distances, build = build)
  return(rf_all)
}

rf_all <- build_df("pratchet", "flu")
rf_all <- rbind(rf_all, build_df("pml", "flu"))
rf_all <- rbind(rf_all, build_df("dnapars", "flu"))
rf_all <- rbind(rf_all, build_df("dnaml", "flu"))



build_df_bs <- function(build, data_source){
  distances <- c(bs_norm(0, build, data_source, "Heavy"), bs_norm(0.95, build, data_source, "Heavy and Light"), bs_norm(0.9, build, data_source, "Heavy and Light"),
                 bs_norm(0.85, build, data_source, "Heavy and Light"), bs_norm(0.8, build, data_source, "Heavy and Light"), bs_norm(0.7, build, data_source, "Heavy and Light"),
                 bs_norm(0.6, build, data_source, "Heavy and Light"), bs_norm(0.5, build, data_source, "Heavy and Light"), bs_norm(0.4, build, data_source, "Heavy and Light"),
                 bs_norm(0.3, build, data_source, "Heavy and Light"), bs_norm(0.2, build, data_source, "Heavy and Light"), bs_norm(0.15, build, data_source, "Heavy and Light"),
                 bs_norm(0.1, build, data_source, "Heavy and Light"), bs_norm(0.05, build, data_source, "Heavy and Light"), bs_norm(0, build, data_source, "Heavy and Light")) 
  props <- c(0,5,10,15,20,30,40,50,60,70,80,85,90,95,100)
  bs_all <- data.frame(props=props, distances=distances, build = build)
  return(bs_all)
}
bs_all <- build_df_bs("pratchet", "flu")
bs_all <- rbind(bs_all, build_df_bs("pml", "flu"))
bs_all <- rbind(bs_all, build_df_bs("dnapars", "flu"))
bs_all <- rbind(bs_all, build_df_bs("dnaml", "flu"))

ggplot(bs_all, aes(x=props, y=distances))+
  theme_bw() +
  geom_line(aes(color = build), size = 2) +
  geom_point(aes(color=build), size = 7) +
  scale_color_manual(values=c("#002B5C", "#00471B", "#F9A01B", "#3E2680")) +
  ylab("RF Distance") +
  xlab("Amount of Light Chain Included")


