#!/bin/bash

# set max wallclock time
#SBATCH --time=99:00:00

# set the number of nodes
#SBATCH --nodes=1

# memory requirement
#SBATCH --mem=180g

# CPUs allocated to each task
#SBATCH --cpus-per-task=20

# mail alert at start, end and abortion of execution
#SBATCH --mail-type=BEGIN,END,FAIL --mail-user=cole.jensen@yale.edu

#SBATCH -p pi_kleinstein

module load R/4.1.0-foss-2020b

scale=$1
nprocs=$2
proportion_light=$3
bootstrap_amount=$4

Rscript sim_v5.R $scale $nprocs $proportion_light $bootstrap_amount