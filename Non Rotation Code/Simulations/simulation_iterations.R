####### script to simulate x iterations of the 124 nosel data ######
library(shazam)
library(alakazam)
library(igraph)
library(dowser)
library(ape)
library(dplyr)
library(phangorn)

# get your baseline parameters 
mutational_rate = 2
lc_model = "HKL_S5F"
nprocs = as.numeric(args[1])
quiet = as.numeric(args[2])
data_source = "125_nosel_heavy"
iterations = as.numeric(args[3])

# read in the custom shmulate function
shmulateTreeNew = function (sequence, graph, targetingModel = HH_S5F, field = NULL, 
                            exclude = NULL, junctionWeight = NULL, start = 1, end = nchar(sequence)) 
{
  if (!is(targetingModel, "TargetingModel")) {
    stop(deparse(substitute(targetingModel)), " is not a valid TargetingModel object")
  }
  #mrca_df <- alakazam::getMRCA(graph, path = "distance", root = "Germline", 
  #    field = field, exclude = exclude)
  # Need to keep adjacency and weight separate. Otherwise zero length branches will be cut
  adj <- as_adjacency_matrix(graph, sparse = FALSE)
  weight <- as_adjacency_matrix(graph, attr = "weight", sparse = FALSE)
  skip_names <- c()
  if (!is.null(field)) {
    g <- vertex_attr(graph, name = field)
    g_names <- vertex_attr(graph, name = "name")
    skip_names <- g_names[g %in% exclude]
  }
  sim_tree <- data.frame(matrix(NA, ncol = 3, nrow = length(V(graph)), 
                                dimnames = list(NULL, c("name", "sequence", "distance"))))
  sim_tree$name <- vertex_attr(graph, name = "name")
  #sim_tree <- sim_tree[-which(sim_tree$name == "Germline"), 
  #    ]
  #parent_nodes <- mrca_df$name[1]
  parent_nodes = "Germline"
  nchild <- sum(adj[parent_nodes, ] > 0)
  sim_tree$sequence[which(sim_tree$name == parent_nodes)] <- sequence
  sim_tree$distance[which(sim_tree$name == parent_nodes)] <- 0
  if (!is.null(junctionWeight)) {
    weight[parent_nodes, ] <- round(weight[parent_nodes, ] * (1 + 
                                                                junctionWeight))
  }
  while (nchild > 0) {
    new_parents <- c()
    for (p in parent_nodes) {
      children <- colnames(adj)[adj[p, ] > 0]
      for (ch in children) {
        new_parents <- union(new_parents, ch)
        new_weight <- rpois(1, weight[p, ch])
        seq <- shmulateSeq(sequence = sim_tree$sequence[sim_tree$name == 
                                                          p], numMutations = new_weight, targetingModel = targetingModel, 
                           start = start, end = end)
        chRowIdx = which(sim_tree$name == ch)
        sim_tree$sequence[chRowIdx] <- seq
        sim_tree$distance[chRowIdx] <- new_weight
      }
    }
    parent_nodes <- new_parents
    nchild <- sum(adj[parent_nodes, ] > 0)
  }
  sim_tree <- sim_tree[!(sim_tree$name %in% skip_names), ]
  sim_tree <- sim_tree[!is.na(sim_tree$sequence), ]
  rownames(sim_tree) <- NULL
  return(sim_tree)
}

# get the data
print("read in the data")
naive_heavy_chain <- readChangeoDb("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/bcr-phylo/starting_data/naive_heavy_chain.tsv")
naive_heavy_chain <- filter(naive_heavy_chain, !grepl("-", sequence_alignment))
naive_light_chain <- readChangeoDb("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/bcr-phylo/starting_data/naive_light_chain.tsv")
naive_light_chain <- filter(naive_light_chain, !grepl("-", sequence_alignment))
original_trees = readRDS(paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/bcr-phylo/starting_data/og_", data_source, ".rds"))
bob <- rtree(1, TRUE, tip.label = "Germline")
bob$edge.length <- 0
for(i in 1:length(original_trees)){
  original_trees[[i]] <- bind.tree(bob, original_trees[[i]])
  original_trees[[i]]$edge_type <- "mutations"
}

print("data successfully read in")

for(iter in 1:iterations){
  if(quiet >0){
    print("making graphs")
  }
  graphs = list()
  for (i in 1:length(original_trees)){
    tree <- original_trees[[i]]
    tree$edge.length = round(tree$edge.length) #+ 1
    graphs[[i]] <- phyloToGraph(tree) #puts the trees into Igraph format
  }
  
  graphs_half = list()
  for (i in 1:length(original_trees)){
    tree <- original_trees[[i]]
    tree$edge.length = ceiling(tree$edge.length/mutational_rate)
    graphs_half[[i]] <- phyloToGraph(tree) #puts the trees into Igraph format
  }
  
  graphs[sapply(graphs, is.null)] <- NULL
  if(quiet > 0){
    print("graphs complete")
  }
  
  
  # Filter out so that only the new_cell_ids that are in the heavy and light chain data are included in the set
  if(quiet > 0){
    print("filter the chains so that there are no chains with gaps")
  }
  naive_cell_id_set <- filter(naive_light_chain, new_cell_id %in% naive_heavy_chain$new_cell_id)
  naive_cell_id_set <- naive_cell_id_set$new_cell_id
  naive_cell_id_set <- unique(naive_cell_id_set)
  if(quiet > 0){
    print("filtering complete")
    print("mutate the trees -- create FRANKENSTEIN")
  }
  
  # for each tree, select random heavy and light chain, simulate, process
  # get each from a naive b cell from the combined dataset
  mutated_tree_list = list()
  
  # create Frankenstein database using the mutated_list
  
  frankenstein_heavy_db <- data.frame(matrix(ncol=ncol(naive_heavy_chain), nrow=0))
  frankenstein_light_db <- data.frame(matrix(ncol=ncol(naive_light_chain), nrow=0))
  colnames(frankenstein_heavy_db) <- colnames(naive_heavy_chain)
  colnames(frankenstein_light_db) <- colnames(naive_light_chain)
  
  for (ii in 1:length(graphs)) {
    #print(ii)
    phylo_tree = graphs[[ii]]
    half_tree = graphs_half[[ii]]
    find_starter <- igraph::as_data_frame(phylo_tree) # get the naive seq that bcr phylo used
    find_starter <- find_starter$to[nrow(find_starter)]
    find_starter <- gsub("\\|.*", "", find_starter)
    temp_seq_h = naive_heavy_chain[naive_heavy_chain$sequence_id == find_starter,][1,] #gets heavy chain using seq id
    temp_seq_l = naive_light_chain[naive_light_chain$new_cell_id == temp_seq_h$new_cell_id,][1,] #gets light chain using heavy chain new cell id
    temp_sim_tree_h = shmulateTreeNew(temp_seq_h$sequence_alignment, phylo_tree, targetingModel = HH_S5F)            #simulate the  heavy chain mutations
    # simulate the light chain mutations based on what model input is used
    if(lc_model == "HH_S5F"){
      temp_sim_tree_l = shmulateTreeNew(temp_seq_l$sequence_alignment, half_tree, targetingModel = HH_S5F)
    } else{
      temp_sim_tree_l = shmulateTreeNew(temp_seq_l$sequence_alignment, half_tree, targetingModel = HKL_S5F)           
    }
    
    
    mutated_tree_list[[find_starter]] = c(temp_sim_tree_h, temp_sim_tree_l) #store the simulated trees just in case
    
    #make an entry in the new database for each mutated sequence
    for (ind in 1:length(temp_sim_tree_h$sequence)){
      if (nchar(temp_sim_tree_h$name[ind]) > 6){ #as long as inferred isn't in the sequence
        temp_heavy_row = temp_seq_h[rep(1,1), ] # make copy of heavy row
        temp_light_row = temp_seq_l[rep(1,1), ] # make copy of light row
        
        temp_seq_align = temp_heavy_row$sequence_alignment
        temp_heavy_row$sequence_alignment = temp_sim_tree_h$sequence[ind]     # mutated sequence
        temp_heavy_row$germline_alignment = temp_seq_align                    # original sequence
        temp_heavy_row$sequence_id = temp_sim_tree_h$name[ind]                # sequence_id taken from tree
        temp_heavy_row$clone_id = ii               # clone_id taken from tree number -- indicating clone id from bcr-phylo
        
        tmp_str = sapply(strsplit(temp_sim_tree_h$name[ind], "-"), "[", 1)
        tmp_cell_id = paste(tmp_str, temp_heavy_row$clone_id, sep="-")
        
        temp_heavy_row$cell_id = tmp_cell_id                                  # cell_id = first part of seq_id plus clone number
        frankenstein_heavy_db <- rbind(frankenstein_heavy_db, temp_heavy_row) # add the temporary row to the data frame
        
        temp_seq_align = temp_light_row$sequence_alignment
        temp_light_row$sequence_alignment = temp_sim_tree_l$sequence[ind]     # mutated sequence
        temp_light_row$germline_alignment = temp_seq_align                    # original sequence
        temp_light_row$sequence_id = temp_sim_tree_l$name[ind]                # sequence_id taken from tree
        temp_light_row$cell_id = tmp_cell_id                                  # cell_id same as heavy chain
        temp_light_row$clone_id = ii                # clone_id taken from tree number -- indicating clone id from bcr-phylo
        frankenstein_light_db <- rbind(frankenstein_light_db, temp_light_row) # add the temporary row to the data frame
      }
    }
  }
  
  if(quiet > 0){
    print("the mutated trees have been made")
    print("frankenstein heavy and light dbs created")
  }
  
  
  frankenstein_heavy_db$sequence_id <- paste0(frankenstein_heavy_db$sequence_id, "_", frankenstein_heavy_db$clone_id)
  frankenstein_light_db$sequence_id <- paste0(frankenstein_light_db$sequence_id, "_", frankenstein_light_db$clone_id)
  frankenstein_heavy_db$cell_id <- frankenstein_heavy_db$sequence_id
  frankenstein_light_db$cell_id <- frankenstein_light_db$sequence_id
  
  # remove the cells with multiple heavy chains 
  heavy_count = table(frankenstein_heavy_db$cell_id)
  sum(heavy_count > 1)
  multi_heavy_cells = names(heavy_count[heavy_count > 1])
  frankenstein_heavy_db = filter(frankenstein_heavy_db, !cell_id %in% multi_heavy_cells)
  rm(heavy_count, multi_heavy_cells)
  
  if(quiet > 0){
    print("getting sub clones and combining heavy and light chain data")
  }
  comb = getSubclones(frankenstein_heavy_db, frankenstein_light_db, cell="cell_id", nproc=nprocs)
  # shouldn't do anything but just in case
  comb = comb[which(!is.na(comb$c_call)),]
  if(quiet > 0){
    print("combining complete")
    print("formatting clones")
  }
  # update for
  references <- readIMGT("/media/cole/Eris/programs/imgt/human/vdj")
  comb$sequence_id <- paste0(comb$sequence_id, "_", comb$locus)
  comb <- filter(comb, !grepl("Germline", sequence_id))
  hlclones = formatClones(comb,chain="HL",
                          split_light=TRUE,heavy="IGH",cell="cell_id",
                          minseq = 3, collapse = TRUE, nproc=nprocs, germ = "germline_alignment") 
  # filter out the comb to make sure that we don't collapse sequences that are different from hlclones
  seq_names <- c()
  for(i in 1:nrow(hlclones)){
    to_bind <- hlclones$data[[i]]@data$sequence_id
    seq_names <- append(seq_names, to_bind)
  }
  comb_germline_filtered <- filter(comb, sequence_id %in% seq_names)
  hclones = formatClones(comb_germline_filtered,chain="H",
                         split_light=TRUE,heavy="IGH",cell="cell_id",
                         minseq = 3, collapse = FALSE, nproc=nprocs)
  #rm(bob, frankenstein_heavy_db, frankenstein_light_db, graphs, graphs_half, half_tree, mutated_tree_list, naive_cell_id_set, naive_heavy_chain, naive_light_chain, original_trees_filtered, phylo_tree, references, temp_heavy_row, temp_light_row, temp_seq_align, temp_seq_h, temp_seq_l, temp_sim_tree_h, temp_sim_tree_l, cut_off, i, ii, ind, lc_model, mutational_rate, tmp_cell_id, tmp_str, seq_names, to_bind, comb_germline_filtered, lineage_tree, tree, num_seq, find_starter)
  
  saveRDS(hlclones, paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/bcr-phylo/sim_iters/hlclones_", data_source, "_", iter,".rds"))
  saveRDS(hclones, paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/bcr-phylo/sim_iters/hclones_", data_source, "_", iter,".rds"))
  saveRDS(comb, paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/bcr-phylo/sim_iters/comb_", data_source, "_", iter,".rds"))
}
print("It's done. You can start the RF tests now.")