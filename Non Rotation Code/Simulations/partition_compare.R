library(dowser)

# set up the masking function 
random_chains <- function(clones, proportion_masked){
  for(i in 1:nrow(clones)){
    light_index = clones$data[[i]]@locus != "IGH"
    heavy_index = clones$data[[i]]@locus == "IGH"
    results <- c()
    for (seq in 1:length(clones$data[[i]]@data$hlsequence)) {
      results[seq] <- sample(c(1,0), replace = F, prob = c(proportion_masked, 1-proportion_masked), size = 1)
    }
    for(idx in 1:length(results)){
      if(results[idx] == 1){
        heavy_chain <- paste(strsplit(clones$data[[i]]@data$hlsequence,split="")[[idx]][heavy_index], collapse = "")
        light_chain <- paste(strsplit(clones$data[[i]]@data$hlsequence,split="")[[idx]][light_index], collapse = "")
        new_light_chain <- paste(strsplit(rep("N", nchar(light_chain)), split=""), collapse = "")
        new_hlseq <- gsub(" ", "", paste(heavy_chain, new_light_chain, collapse=""), fixed=TRUE)
        clones$data[[i]]@data$hlsequence[idx] <- new_hlseq
      } else{
        clones$data[[i]]@data$hlsequence[idx] <- clones$data[[i]]@data$hlsequence[idx]
      }
    }
  }
  return(clones)
}


# compare the to real edge lengths
#og_sum <- sum(og_tree[[1]]$edge.length)
find_diff <- function(og_sum, new_tree){
  new_sum <- sum(new_tree$edge.length)
  diff <- new_sum - og_sum 
  return(diff)
}

# read in the data 
clones <- readRDS("~/Downloads/hlclones_125_nosel_1.rds")
og_tree <- readRDS("~/Downloads/og_125_nosel.rds")
# clean up clones to be nice and pretty 
clones$clone_id <- as.numeric(gsub("\\_.*", "", clones$clone_id))
clones <- clones[order(clones$clone_id),]

df <- c()
for(i in 1:nrow(clones)){
  clones_masked <- random_chains(clones[i,], 0.5)
  
  # igphyml tree
  ig_tree <- getTrees(clones[i,], build="igphyml", partition="hl", dir="~/Documents/raxml_testing",id="hl",optimize="tlr",
                      exec="/media/cole/Eris/programs/igphyml/src/igphyml",omega="e,e", rates="1,e")
  ig_tree_masked <- getTrees(clones_masked[i,], build="igphyml", partition="hl", dir="~/Documents/raxml_testing",id="hl",optimize="tlr",
                             exec="/media/cole/Eris/programs/igphyml/src/igphyml", omega="e,e", rates="1,e")
  ig_tree <- scaleBranches(ig_tree)
  ig_tree_masked <- scaleBranches(ig_tree_masked)
  # raxml unlinked
  unlinked_rax <- getTrees(clones[i,], build = "raxml", exec = "/media/cole/Eris/programs/raxml-ng/raxml-ng", partition = TRUE, brln = "unlinked")
  unlinked_rax_masked <- getTrees(clones_masked[i,], build = "raxml", exec = "/media/cole/Eris/programs/raxml-ng/raxml-ng", partition = TRUE, brln = "unlinked")
  unlinked_rax <- scaleBranches(unlinked_rax)
  unlinked_rax_masked <- scaleBranches(unlinked_rax_masked)
  # raxml linked
  linked_rax <- getTrees(clones[i,], build = "raxml", exec = "/media/cole/Eris/programs/raxml-ng/raxml-ng", partition = TRUE, brln = "linked")
  linked_rax_masked <- getTrees(clones_masked[i,], build = "raxml", exec = "/media/cole/Eris/programs/raxml-ng/raxml-ng", partition = TRUE, brln = "linked")
  linked_rax <- scaleBranches(linked_rax)
  linked_rax_masked <- scaleBranches(linked_rax_masked)
  # raxml scaled
  scaled_rax <- getTrees(clones[i,], build = "raxml", exec = "/media/cole/Eris/programs/raxml-ng/raxml-ng", partition = TRUE, brln = "scaled")
  scaled_rax_masked <- getTrees(clones_masked[i,], build = "raxml", exec = "/media/cole/Eris/programs/raxml-ng/raxml-ng", partition = TRUE, brln = "scaled")
  scaled_rax <- scaleBranches(scaled_rax)
  scaled_rax_masked <- scaleBranches(scaled_rax_masked)
  # DNAPARS 
  dnapars <- getTrees(clones[i,], build = "dnapars", exec = "/media/cole/Eris/programs/phylip-3.697/exe/dnapars")
  dnapars_masked <- getTrees(clones_masked[i,], build = "dnapars", exec = "/media/cole/Eris/programs/phylip-3.697/exe/dnapars")
  dnapars <- scaleBranches(dnapars)
  dnapars_masked <- scaleBranches(dnapars_masked)
  
  # get the row of the final df
  og_sum <- 1.5*sum(og_tree[[i]]$edge.length)
  tobind <- data.frame(clone_id = clones[i,]$clone_id,
                       expected = og_sum, 
                       igphyml = find_diff(og_sum, ig_tree$trees[[i]]),
                       igphyml_masked = find_diff(og_sum, ig_tree_masked$trees[[i]]),
                       raxml_scaled = find_diff(og_sum, scaled_rax$trees[[i]]),
                       raxml_scaled_masked = find_diff(og_sum, scaled_rax_masked$trees[[i]]),
                       raxml_unlinked = find_diff(og_sum, unlinked_rax$trees[[i]]),
                       raxml_unlinked_masked = find_diff(og_sum, unlinked_rax_masked$trees[[i]]),
                       raxml_linked = find_diff(og_sum, linked_rax$trees[[i]]),
                       raxml_linked_masked = find_diff(og_sum, linked_rax_masked$trees[[i]]),
                       dnapars = find_diff(og_sum, dnapars$trees[[i]]),
                       dnapars_masked = find_diff(og_sum, dnapars_masked$trees[[i]]))
  
  df <- rbind(df, tobind)
}

saveRDS(df, "~/Downloads/look_at_me.rds")
