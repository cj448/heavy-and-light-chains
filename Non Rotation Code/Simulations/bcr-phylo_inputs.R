# this is a script to write the combined fasta file
# for the heavy and light chain data used in BCR-PHYLO
# the header/genome name will be length HC | length LC
# this way you can parse out the data afterwards
library(alakazam)
library(dplyr)
library(seqinr)
library(stringr)


heavy <- readChangeoDb("/media/cole/Eris/Jake Code/naive_heavy_chain.tsv")
light <- readChangeoDb("/media/cole/Eris/Jake Code/naive_light_chain.tsv")

# limit the light chains to only one per heavy chain and vice versa 
heavy_count = table(heavy$cell_id)
sum(heavy_count > 1)
multi_heavy_cells = names(heavy_count[heavy_count > 1])
heavy = filter(heavy, !cell_id %in% multi_heavy_cells)
light_count = table(light$cell_id)
sum(light_count > 1)
multi_light_cells = names(light_count[light_count > 1])
light = filter(light, !cell_id %in% multi_light_cells)
heavy <- filter(heavy, cell_id %in% light$cell_id)
light <- filter(light, cell_id %in% heavy$cell_id)
rm(heavy_count, light_count, multi_heavy_cells, multi_light_cells)

# sort both the dfs by cell_id so they line up
heavy <- heavy[order(heavy$cell_id),]
light <- light[order(light$cell_id),]

# do a sanity check 
table(heavy$cell_id == light$cell_id)

# testing that it works
#heavy_c <- gsub("\\.\\.\\.", "", light$sequence_alignment[1])
#print(translateDNA(heavy_c))
#len_hc <- nchar(heavy_c)/3 #.3333, .6667
#heavy_c <- gsub(" ", "", paste(heavy_c, "NN"))
#test_hc <- nchar(heavy_c)/3
#test <- paste0(heavy$sequence_alignment[1], light$sequence_alignment[1])
#test <- gsub("\\.\\.\\.", "", test)
#if(str_detect(test, "\\.") == TRUE){
#  test <- gsub("\\.", "N", test)
#}


# okay so write the file 
sink("~/Documents/Simulations/bcr-phylo/naive.fasta")
for(i in 1:nrow(heavy)){
  heavy_c <-  gsub("\\.\\.\\.", "", heavy$sequence_alignment[i])
  if(str_detect(heavy_c, "\\.") == TRUE){
    heavy_c <- gsub("\\.", "N", heavy_c)
  }
  if((nchar(heavy_c)/3)%%1!=0){
    len_hc <- nchar(heavy_c)/3
    if(str_detect(len_hc, ".3333")){
      heavy_c <- gsub(" ", "", paste(heavy_c, "NN"))
    }else if(str_detect(len_hc, ".6667")){
      heavy_c <- gsub(" ", "", paste(heavy_c, "N"))
    }
  }
  light_c <-  gsub("\\.\\.\\.", "", light$sequence_alignment[i])
  if(str_detect(light_c, "\\.") == TRUE){
    light_c <- gsub("\\.", "N", light_c)
  }
  if((nchar(light_c)/3)%%1!=0){
    len_hc <- nchar(light_c)/3
    if(str_detect(len_hc, ".3333")){
      light_c <- gsub(" ", "", paste(light_c, "NN"))
    }else if(str_detect(len_hc, ".6667")){
      light_c <- gsub(" ", "", paste(light_c, "N"))
    }
  }
  to_cat <- paste0(heavy_c, light_c)
  cat(paste0(">", nchar(heavy_c), "|", nchar(light_c), "\n"))
  cat(paste0(to_cat, "\n"))
}
sink()
closeAllConnections()

# check if you made stop codons
naives <- read.fasta("~/Documents/Simulations/bcr-phylo/naive.fasta")
#bob <- toupper(getSequence(naives[1], as.string = TRUE)[[1]][[1]])

stop_codons <- c()
for(i in 1:length(naives)){
  seq <- toupper(getSequence(naives[i], as.string = TRUE)[[1]][[1]])
  test <- translateDNA(seq)
  if(grepl("\\*", test)){
    stop_codons <- append(stop_codons, i)
  }
}


####################################################################################
################################# Run BCR-PHYLO ####################################
####################################################################################

# to run bcr-phylo first download the repo (I used committ b2902d7)
# you can find the repo at https://github.com/matsengrp/bcr-phylo-benchmark
# change the following to the simulator.py script
# on line 603 change the node.name to be 'node.name = args.naive_seq_name'
# on line 613 change the leaf.name to be 'leaf.name = args.naive_seq_name'
# on line 873 add parser.add_argument('--naive_seq_name', default='', help='sequence identifer')
# on line 904 add args.naive_seq_name = str(records[0].name)

# then you can run it bash gcSim.sh script that Ken wrote
# I did update the simulateGC.py script to be inline with my version of the pipeline and 
# pull from my datasources (like the naive fasta file made).

# to run it just do bash gcSim.sh and your options while in the folder of the script.

###################################################################################
############################### Deparse the data ##################################
###################################################################################



