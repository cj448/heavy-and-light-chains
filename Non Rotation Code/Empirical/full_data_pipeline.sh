#!/bin/bash

# set max wallclock time
#SBATCH --time=99:00:00

# set the number of nodes
#SBATCH --nodes=1

# memory requirement
#SBATCH --mem=150g

# CPUs allocated to each task
#SBATCH --cpus-per-task=36

# mail alert at start, end and abortion of execution
#SBATCH --mail-type=BEGIN,END,FAIL --mail-user=cole.jensen@yale.edu

#SBATCH -p pi_kleinstein

module load R/4.1.0-foss-2020b

nprocs=$1
proportion_light=$2
bootstrap_amount=$3
data=$4
quiet=$5
build=$6


Rscript empirical_full.R $nprocs $proportion_light $bootstrap_amount $data $quiet $build
