#!/bin/bash

#SBATCH -p pi_kleinstein

sbatch mild_covid_bootstrapping.sh 36 0 100
sbatch mild_covid_bootstrapping.sh 36 0.100 100
sbatch mild_covid_bootstrapping.sh 36 0.5 100
sbatch mild_covid_bootstrapping.sh 36 0.75 100
sbatch mild_covid_bootstrapping.sh 36 1 100