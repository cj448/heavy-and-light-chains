################################################################################
################# Empirical Data Script ########################################
################################################################################
# This is script will go through the real data and do
# The light chain masking
# The bootstrapping

# I'll need to to add some dynamic naming to get things saved
# The naming will be based off of the commandArgs
library(dplyr)
library(dowser)
library(alakazam)
library(scoper)
library(igraph)
library(shazam)
library(phangorn)
library(ggtree)
library(ggplot2)
library(ggpubr)
library(ape)
library(gridExtra)
library(devtools)
library(parallel)
library(PairedData)

#  input user level parameters
args = commandArgs(trailingOnly=TRUE)
nproc = 18
proportion_light = 1
bootstrap_amount <-  1
data <- "flu"
quiet <- 5
build <- "pml"

# create the exec variable 
if(build=="dnapars"){
  exec <- paste("/media/cole/Eris/programs/phylip-3.697/exe/dnapars")
} else if (build =="dnaml"){
  exec <- paste("/media/cole/Eris/programs/phylip-3.697/exe/dnaml")
} else if(build == "igphml"){
  exec <- paste("/media/cole/Eris/programs/igphyml/src/igphyml")
} else{
  exec <- NULL
}

# set the starting working directory to ensure that you're saving the data where you want it
setwd("~/Documents/dowser_testing")

print(paste0("reading in data for ", data, " bootstrapping"))

# get the input data based on the data input variable 
if(data == "mild_covid"){
  heavy <- readChangeoDb("/media/cole/Eris/data/Mild Covid/results_heavy_db.tsv")
  light <- readChangeoDb("/media/cole/Eris/data/Mild Covid//results_light_db.tsv")
  print("read in the heavy and light data")
  
  heavy$new_cell_id = paste0(heavy$sample, "-", heavy$cell_id)
  light$new_cell_id = paste0(light$sample, "-", light$cell_id)
  
  heavy_count = table(heavy$new_cell_id)
  sum(heavy_count > 1)
  multi_heavy_cells = names(heavy_count[heavy_count > 1])
  heavy = filter(heavy, !new_cell_id %in% multi_heavy_cells)
  print("heavy and light data finished reformating")
  #You'll also need to specify cell="new_cell_id" for getSubclones and formatClones to ensure unique cell ids are used.
  
  comb = getSubclones(heavy, light, cell="new_cell_id", nproc=nproc)
  
  comb$clone_id <- paste0(comb$clone_id, "-", comb$subclone_id)
  print("comb created")
  
  print('reading in references and creating, h, k, and l files')
  references = readIMGT(dir = "/media/cole/Eris/programs/imgt//human/vdj")
  #h = createGermlines(filter(comb,locus=="IGH"),references, nproc = nproc)
  #k = createGermlines(filter(comb,locus=="IGK"),references,locus="IGK", nproc = nproc)
  #l = createGermlines(filter(comb,locus=="IGL"),references,locus="IGL", nproc = nproc)
  #print("h, l, k, created")
  #comb_germline = bind_rows(h, l, k)
  comb_germline = createGermlines(comb, references, locus="locus", nproc = nproc)
  comb_germline = comb_germline[which(!is.na(comb_germline$c_call)),]
  print("comb_germline created")
  # if you want to save non airr columsn use columns = c()
  hlclones = formatClones(comb_germline,chain="HL",
                          split_light=TRUE,heavy="IGH",cell="new_cell_id",
                          trait=c("vj_gene","c_call"), minseq = 3, collapse = TRUE, nproc=nproc)
  # now filter the comb_germline to make sure that you get the same sequence_ids for the hclones
  seq_names <- c()
  for(i in 1:nrow(hlclones)){
    to_bind <- hlclones$data[[i]]@data$sequence_id
    seq_names <- append(seq_names, to_bind)
  }
  comb_germline_filtered <- filter(comb_germline, sequence_id %in% seq_names)
  hclones = formatClones(comb_germline_filtered,chain="H",
                         split_light=TRUE,heavy="IGH",cell="new_cell_id",
                         minseq = 3, collapse = FALSE, nproc=nproc)
  # check to see if each hclone seq is unique
  # check to see if dnaml is giving out binary trees (is binary command in ape or something)
  rm(heavy, light, heavy_count, multi_heavy_cells, seq_names, references)
} else if(data == "severe_covid"){
  input_data <- readChangeoDb("/media/cole/Eris/data/covid/covid19_final_processed-all_cols.tab")
  # rename the sequence_ids
  input_data$sequence_id <- paste0(input_data$sequence_id, "_", input_data$sample)
  input_data$subject <- as.character(input_data$subject)
  input_data$new_cell_id <- paste0(input_data$cell_id, "_", input_data$sample, "_", input_data$data_name)
  
  # make the heavy and light input_data
  #KBH need to note in methods that sequecnes with gaps were removed
  heavy <- filter(input_data, locus == "IGH")
  heavy <- filter(heavy, !grepl("-", sequence_alignment))
  light <- filter(input_data, locus != "IGH")
  light <- filter(light, !grepl("-", sequence_alignment))
  print("data sucessfully read in")
  
  heavy_count = table(heavy$new_cell_id)
  sum(heavy_count > 1)
  multi_heavy_cells = names(heavy_count[heavy_count > 1])
  heavy = filter(heavy, !new_cell_id %in% multi_heavy_cells)
  rm(heavy_count, multi_heavy_cells)
  
  print("create the combined file")
  comb = getSubclones(heavy,light,cell="new_cell_id", nproc= nproc)
  # remove the entries where c_call is missing
  comb = comb[which(!is.na(comb$c_call)),]
  
  print('reading in references and creating, h, k, and l files')
  references = readIMGT(dir = "/media/cole/Eris/programs/imgt/human/vdj")
  comb_germline = createGermlines(comb, references, locus="locus", nproc = nproc)
  comb_germline = comb_germline[which(!is.na(comb_germline$c_call)),]
  print("comb_germline created")
  # minseq use it
  hlclones = formatClones(comb_germline,chain="HL",
                          split_light=TRUE,heavy="IGH",cell="new_cell_id",
                          trait=c("vj_gene","c_call"), minseq = 3, collapse = TRUE, nproc=nproc)
  # now filter the comb_germline to make sure that you get the same sequence_ids for the hclones
  seq_names <- c()
  for(i in 1:nrow(hlclones)){
    to_bind <- hlclones$data[[i]]@data$sequence_id
    seq_names <- append(seq_names, to_bind)
  }
  comb_germline_filtered <- filter(comb_germline, sequence_id %in% seq_names)
  hclones = formatClones(comb_germline_filtered,chain="H",
                         split_light=TRUE,heavy="IGH",cell="new_cell_id",
                         minseq = 3, collapse = FALSE, nproc=nproc)

  #KBH need to check that the heavy chain sequences are the same at the nucleotide level for both hclones and hlclones
  #the differences in collapsing operations could cause there to be slightly different sequences
  #between the objects
  rm(heavy, light, input_data, seq_names, references)
} else if(data == "flu"){
  heavy_db <- readChangeoDb("/media/cole/Eris/data/Flu Data/heavy.tsv") 
  light_db <- readChangeoDb("/media/cole/Eris/data/Flu Data/light.tsv") 
  print("ensuring that the heavy chain file isn't troublesome")
  heavy_count = table(heavy_db$cell_id_unique)
  sum(heavy_count > 1)
  multi_heavy_cells = names(heavy_count[heavy_count > 1])
  heavy_db = filter(heavy_db, !cell_id_unique %in% multi_heavy_cells)
  # remove this since we don't need it anymore
  print('Ensuring complete. Congrats')
  print('removing unneeded variables')
  rm(heavy_count, multi_heavy_cells)
  print('creating the germlines')
  print('making the combined subclones')
  comb = getSubclones(heavy_db, light_db, cell="cell_id_unique", nproc = nproc)
  print('comb file complete')
  
  print('reading in references and creating, h, k, and l files')
  references = readIMGT(dir =  "/media/cole/Eris/programs/imgt/human/vdj")
  comb_germline <- createGermlines(comb, references, locus="locus", nproc=nproc)
  comb_germline = comb_germline[which(!is.na(comb_germline$c_call)),]
  print('germline complete')
  
  #KBH need to note and justify differences in minimum sequence count between datasets in the methods
  print('formatting clones')
  print("starting to format hlclones")
  hlclones = formatClones(comb_germline,chain="HL",
                          split_light=TRUE,heavy="IGH",cell="cell_id_unique",
                          trait=c("vj_gene","c_call"), minseq = 10, collapse = TRUE, nproc = nproc)
  print("hlclones formatting complete")
  
  # now filter the comb_germline to make sure that you get the same sequence_ids for the hclones
  seq_names <- c()
  for(i in 1:nrow(hlclones)){
    to_bind <- hlclones$data[[i]]@data$sequence_id
    seq_names <- append(seq_names, to_bind)
  }
  comb_germline_filtered <- filter(comb_germline, sequence_id %in% seq_names)
  print("starting to format hclones")
  hclones = formatClones(comb_germline_filtered,chain="H",
                         split_light=TRUE,heavy="IGH",cell="cell_id_unique", trait=c("vj_gene","c_call"),
                         minseq = 10, collapse = FALSE, nproc=nproc)
  rm(heavy_db, light_db, seq_names, references)
} else{
  stop("data source not recognized! Either update the script or check your spelling")
}

# move the working directory to save all the intermediate data 
if(!dir.exists(file.path("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/intermediate_data"))){
  dir.create(file.path("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/intermediate_data"))
  setwd("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/intermediate_data")
} else{
  setwd("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/intermediate_data")
}

comb_name <- paste0("comb_", data, "_", proportion_light, "_", build, ".rds")
comb_germline_name <- paste0("comb_germlines_", data, "_", proportion_light, "_", build, ".rds")
hclones_name <- paste0("hclones_", data, "_", proportion_light, "_", build, ".rds")
hlclones_name <- paste0("hlclones_", data, "_", proportion_light, "_", build, ".rds")

saveRDS(comb, comb_name)
saveRDS(comb_germline, comb_germline_name)
saveRDS(hclones, hclones_name)
saveRDS(hlclones, hlclones_name)

rm(comb_germline_name, comb_name, hlclones_name, hclones_name, comb, comb_germline)

if(!dir.exists(file.path("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping"))){
  dir.create(file.path("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping"))
}

#KBH should be done by pasting proportion_light into the file path
if(proportion_light == 0){
  if(!dir.exists(file.path("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0"))){
    dir.create(file.path("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0"))
    setwd("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0")
    dir_name <- paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0/", data)
  } else{
    setwd("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0")
    dir_name <- paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0/", data)
  }
} else if(proportion_light == 0.25){
  if(!dir.exists(file.path("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0.25"))){
    dir.create(file.path("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0.25"))
    setwd("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0.25")
    dir_name <- paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0.25/", data)
  } else{
    setwd("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0.25")
    dir_name <- paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0.25/", data)
  }
} else if(proportion_light == 0.5){
  if(!dir.exists(file.path("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0.5"))){
    dir.create(file.path("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0.5"))
    setwd("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0.5")
    dir_name <- paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0.5/", data)
  } else{
    setwd("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0.5")
    dir_name <- paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0.5/", data)
  }
} else if(proportion_light == 0.75){
  if(!dir.exists(file.path("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0.75"))){
    dir.create(file.path("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0.75"))
    setwd("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0.75")
    dir_name <- paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0.75/", data)
  } else{
    setwd("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0.75")
    dir_name <- paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_0.75/", data)
  }
} else if(proportion_light == 1){
  if(!dir.exists(file.path("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_1"))){
    dir.create(file.path("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_1"))
    setwd("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_1")
    dir_name <- paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_1/", data)
  } else{
    setwd("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_1")
    dir_name <- paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/bootstrapping/prop_1/", data)
  }
}


print("Randomly removing light chain sequence data")
random_chains <- function(clone_data, selection_prob){
  results <- c()
  for (i in 1:length(clone_data$data[[1]]@data$hlsequence)) {
    results[i] <- sample(c(1,0), replace = F, prob = c(selection_prob, 1-selection_prob), size = 1)
  }
  light_index = clone_data$data[[1]]@locus != "IGH"
  heavy_index = clone_data$data[[1]]@locus == "IGH"
  for(i in 1:length(results)){
    if(results[i] == 1){
      heavy_chain <- paste(strsplit(clone_data$data[[1]]@data$hlsequence,split="")[[i]][heavy_index], collapse = "")
      light_chain <- paste(strsplit(clone_data$data[[1]]@data$hlsequence,split="")[[i]][light_index], collapse = "")
      new_light_chain <- paste(strsplit(rep("N", nchar(light_chain)), split=""), collapse = "")
      new_hlseq <- gsub(" ", "", paste(heavy_chain, new_light_chain, collapse=""), fixed=TRUE)
      clone_data$data[[1]]@data$hlsequence[i] <- new_hlseq
    }
    else{
      clone_data$data[[1]]@data$hlsequence[i] <- clone_data$data[[1]]@data$hlsequence[i]
    }
  }
  return(clone_data)
}

#print("removing random light chains")
hlclones <- lapply(1:length(hlclones$clone_id), function(x)random_chains(hlclones[x,], proportion_light))
hlclones <- do.call(rbind, hlclones)

print("getting trees")
# change this back once done troubleshooting
hclones <- getTrees(hclones, nproc = nproc, build = build, exec = exec)
print("hclone trees done")
hlclones <- getTrees(hlclones, nproc = nproc, build = build, exec = exec)
print("hlclones trees done. Now checking tree quality")


bad_clones <- c()
for(i in 1:nrow(hclones)){
  if(!is.binary(hclones$trees[[i]])){
    bad_clones <- append(bad_clones, hclones$clone_id[i])
  }
}

bad_clones_hl <- c()
for(i in 1:nrow(hlclones)){
  if(!is.binary(hlclones$trees[[i]])){
    bad_clones_hl <- append(bad_clones_hl, hlclones$clone_id[i])
  }
}

if(length(bad_clones) > 0 || length(bad_clones_hl) > 0){
  print("removing bad clones")
}

#KBH need to quanitfy the frequency of these clones
#if all the trees are supposed to be binary but they aren't, then the script should 
#stop and the problem fixed, rather than filtering them out
#could also run multi2di
hclones <- filter(hclones, !clone_id %in% bad_clones)
hclones <- filter(hclones, !clone_id %in% bad_clones_hl)
hlclones <- filter(hlclones, !clone_id %in% bad_clones)
hlclones <- filter(hlclones, !clone_id %in% bad_clones_hl)
rm(bad_clones, bad_clones_hl)
print("trees have been successfully made")
# save the clones with trees
hclones_name <- paste0("hclones", data, "_", proportion_light, "_", build, "_with_trees.rds")
hlclones_name <- paste0("hlclones", data, "_", proportion_light, "_", build, "_with_trees.rds")
saveRDS(hlclones, hlclones_name)
saveRDS(hclones, hclones_name)

print('starting the boostrapping -- this may take awhile')

# naming operation goes mut rate, lc_model, cut off, prop_light
hclone_name <- paste0("plots_hclones", "_", data, "_", proportion_light, "_", build, "_masked.pdf")
hlclone_name <- paste0("plots_hlclones", "_", data, "_", proportion_light, "_", build, "_masked.pdf")
comp_name <- paste0("comp", "_", data, "_",  proportion_light, "_", build, "_masked.csv")
bs_hclones_name<- paste0("hclones", data, "_", proportion_light, "_", build, "_bootstrapped.rds")
bs_hlclones_name<- paste0("hlclones", data, "_", proportion_light, "_", build, "_bootstrapped.rds")

print(paste0('bootstrap amount is ', bootstrap_amount)) 
print('starting the hclones bootstrapping')
hclones <- getBootstraps(hclones, bootstraps = bootstrap_amount, nproc = nproc, bootstrap_nodes = TRUE, quiet=5, build=build, exec = exec, dir = dir_name, asr="none")
print('starting the hlclones bootstrapping')
hlclones <- getBootstraps(hlclones, bootstraps = bootstrap_amount, nproc = nproc, bootstrap_nodes = TRUE, quiet=5, build=build, exec = exec, dir = dir_name, asr="none")

# check to see if/where clones from one are not in the other
#KBH again, this needs to be quantified and the problem potentially fixed. 
#We don't want to just silently throw out data when something goes wrong
hclones <- filter(hclones, clone_id %in% hlclones$clone_id)
hlclones <- filter(hlclones, clone_id %in% hclones$clone_id)
# find the amounts for the table and what not
print('Getting the aveage bootstrapping score by clone id')
hclones$bootstrap_values <- rep(0, nrow(hclones))
hclones_avg <- c()
for(clone in 1:nrow(hclones)){
  matches_from_nodes <- c()
  for(i in 1:length(hclones[clone,]$trees[[1]]$nodes)){
    to_bind <- hclones[clone,]$trees[[1]]$nodes[[i]]$bootstrap_value
    print(to_bind)
    matches_from_nodes <- append(matches_from_nodes, to_bind)
  }
  hclones$bootstrap_values[clone] <- mean(matches_from_nodes)
  hclones_avg <- rbind(hclones_avg, mean(matches_from_nodes))
  print(mean(matches_from_nodes))
}

hlclones$bootstrap_values <- rep(0, nrow(hlclones))
hlclones_avg <- c()
for(clone in 1:nrow(hlclones)){
  matches_from_nodes <- c()
  for(i in 1:length(hlclones[clone,]$trees[[1]]$nodes)){
    to_bind <- hlclones[clone,]$trees[[1]]$nodes[[i]]$bootstrap_value
    print(to_bind)
    matches_from_nodes <- append(matches_from_nodes, to_bind)
  }
  hlclones$bootstrap_values[clone] <- mean(matches_from_nodes)
  hlclones_avg <- rbind(hlclones_avg, mean(matches_from_nodes))
  print(mean(matches_from_nodes))
}

saveRDS(hclones, bs_hclones_name)
saveRDS(hlclones, bs_hlclones_name)

# make the comp file 
comp <- cbind(data.frame(hclones_avg), data.frame(hlclones_avg))

for(i in 1:length(hclones$clone_id)){
  if((comp$hclones_avg[i] > comp$hlclones_avg[i]) == T){
    comp$larger_value[i] <- "hclones"
    comp$l_value[i] <- comp$hclones_avg[i]
  }
  else if((comp$hlclones_avg[i] > comp$hclones_avg[i]) == T){
    comp$larger_value[i] <- "hlclones"
    comp$l_value[i] <- comp$hlclones_avg[i]
  }
  else{
    comp$larger_value[i] <- "same"
    comp$l_value[i] <- comp$hlclones_avg[i]
  }
}

comp$tree_num <- 1:nrow(hlclones)
comp$seq_size <- hlclones$seqs
write.csv(comp, file = comp_name, row.names=FALSE)



# move the working directory to save the figures produced here
if(!dir.exists(file.path("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/figures"))){
  dir.create(file.path("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/figures"))
  setwd("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/figures")
} else{
  setwd("/gpfs/gibbs/pi/kleinstein/heavy_light/project/empirical_data/figures")
}

# create the pdfs
print("make the pdfs")

pdf(hclone_name)
for(i in 1:nrow(hclones)){
  plot(hclones$trees[[i]])
  nodelabels(hclones$bootstrap_values[[i]])
}
dev.off()

pdf(hlclone_name)
for(i in 1:nrow(hlclones)){
  plot(hlclones$trees[[i]])
  nodelabels(hlclones$bootstrap_values[[i]])
}
dev.off()

# df = comp file 
profile <- function(df){
  heavy <- as.data.frame(rep("heavy", length(df$tree_num)))
  hl <- as.data.frame(rep("heavy_light", length(df$tree_num)))
  colnames(heavy) <- c("group")
  colnames(hl) <- c("group")
  my_data <- rbind(heavy, hl)
  hbs_scores <- as.data.frame(df$hclones_avg)
  hlbs_scores <- as.data.frame(df$hlclones_avg)
  colnames(hbs_scores) <- c("score")
  colnames(hlbs_scores) <- c("score")
  bob <- rbind(hbs_scores, hlbs_scores)
  my_data <- cbind(my_data, bob)#, total_branch_length,avg_branch_length)
  cole_palette <- c("#0080C6", "#FFC20E")
  profile <- ggpaired(my_data, x = "group", y = "score",
                      color = "group", line.color = "gray", line.size = 0.4,
                      palette = cole_palette, title = "Profile Graph") +
    xlab("") +
    ylab("") +
    theme(plot.title = element_text(hjust = 0.5, size = 25, face = "bold"),
          axis.text = element_text(size=8))
  return(profile)
}


profile_graph <- profile(comp)
profile_name <- paste0("profile_", data, "_", proportion_light, "_", build, "_masked.jpeg")
jpeg(file=profile_name, type="cairo", height = 1000, width = 1000)
profile_graph
dev.off()
print("IT'S DONE")
