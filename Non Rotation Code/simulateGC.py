from multiprocessing import Pool
import time
import os
import subprocess
import random
import sys
import scipy as sp

def runSimulator(args):

   motifs, naive, sel, clone, times, samples, subject, carry_cap, rseed = [args[i] for i in range(0,len(args))]
   outbase = "/home/cole/Documents/Simulations/bcr-phylo/data/" + subject + "/" + str(clone)

   sim_args = ["/media/cole/Eris/programs/bcr-phylo-benchmark/bin/simulator.py",
   "--mutability_file",motifs+"/Mutability_S5F.csv",
   "--substitution_file",motifs+"/Substitution_S5F.csv",
   "--lambda0", str(0.365),
   #"--no_selection", # If you want nonnatural selection change this to --selection_strength 1
   "--selection_strength", str(selection),
   "--obs_times"," ".join(times),
   "--n_to_sample"," ".join(samples),
   "--metric_for_target_dist", str("aa"),
   "--target_dist", str(15),
   "--target_count",str(1),
   "--carry_cap",str(carry_cap),
   "--debug", str(0),
   "--n_tries",str(1000),
   "--no_context",
   "--no_plot",
   "--outbase", outbase,
   "--naive_seq_file", naive,  
   ">",outbase+".log"
   ]
   out = subprocess.check_call(" ".join(sim_args), shell=True, stdout=True)
   rm_files = [ ]

   if out == 0:
      for file in rm_files:
         os.remove(outbase + file)

if __name__ == "__main__":
    
    args = list()

    cmd_args = sys.argv
    print(cmd_args)

    #first_sample = 10
    nsample = 50
    second_sample = int(cmd_args[1])
    selection = float(cmd_args[2])
    carry_cap = int(cmd_args[3])
    reps = int(cmd_args[4])
    nproc = int(cmd_args[5])

    pool = Pool(nproc)
    
    motifs = "/media/cole/Eris/programs/bcr-phylo-benchmark/motifs"
    naive = "/home/cole/Documents/Simulations/bcr-phylo/naive.fasta"
    subject = str(second_sample) + "-" + str(selection) + "-" + str(carry_cap)
    subprocess.call(["mkdir","/home/cole/Documents/Simulations/bcr-phylo/data/"+subject])

    args = []
    runargs = []
    rseed = 1
    for clone in range(0,reps):
        times   = [str(second_sample)]#, str(120)] # if you want two time points, 120 is the deafult
        samples = [str(nsample)]
        run = [motifs, naive, selection, clone, times, samples, subject, carry_cap, rseed]
        rseed += 1
        args.append(run)

    results = pool.map(runSimulator,args)