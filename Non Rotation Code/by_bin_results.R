library(abind)
library(OpenImageR)
library(ggplot2)
library(dplyr)
library(devtools)


comp_0 <- readRDS(paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/rf_results/results_0_", build, "_", data_source, ".rds"))
comp_5 <- readRDS(paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/rf_results/results_0.05_", build, "_", data_source, ".rds"))
comp_10 <- readRDS(paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/rf_results/results_0.1_", build, "_", data_source, ".rds"))
comp_15 <- readRDS(paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/rf_results/results_0.15_", build, "_", data_source, ".rds"))
comp_20 <- readRDS(paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/rf_results/results_0.2_", build, "_", data_source, ".rds"))
comp_30 <- readRDS(paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/rf_results/results_0.3_", build, "_", data_source, ".rds"))
comp_40 <- readRDS(paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/rf_results/results_0.4_", build, "_", data_source, ".rds"))
comp_50 <- readRDS(paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/rf_results/results_0.5_", build, "_", data_source, ".rds"))
comp_60 <- readRDS(paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/rf_results/results_0.6_", build, "_", data_source, ".rds"))
comp_70 <- readRDS(paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/rf_results/results_0.7_", build, "_", data_source, ".rds"))
comp_80 <- readRDS(paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/rf_results/results_0.8_", build, "_", data_source, ".rds"))
comp_85 <- readRDS(paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/rf_results/results_0.85_", build, "_", data_source, ".rds"))
comp_90 <- readRDS(paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/rf_results/results_0.9_", build, "_", data_source, ".rds"))
comp_95 <- readRDS(paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/rf_results/results_0.95_", build, "_", data_source, ".rds"))

# bins are 0-10, 11-20, 21-30, 31+
sub_comp_0 <- filter(comp_0, seqs <= 10)
sub_comp_5 <- filter(comp_5, seqs <= 10)
sub_comp_10 <- filter(comp_10, seqs <= 10)
sub_comp_15 <- filter(comp_15, seqs <= 10)
sub_comp_20 <- filter(comp_20, seqs <= 10)
sub_comp_30 <- filter(comp_30, seqs <= 10)
sub_comp_40 <- filter(comp_40, seqs <= 10)
sub_comp_50 <- filter(comp_50, seqs <= 10)
sub_comp_60 <- filter(comp_60, seqs <= 10)
sub_comp_70 <- filter(comp_70, seqs <= 10)
sub_comp_80 <- filter(comp_80, seqs <= 10)
sub_comp_85 <- filter(comp_85, seqs <= 10)
sub_comp_90 <- filter(comp_90, seqs <= 10)
sub_comp_95 <- filter(comp_95, seqs <= 10)

props <- c(0, 5,10,15,20,30,40,50,60,70,80,85,90,95,100)
values <- c(mean(sub_comp_0$rf_dist_heavy), mean(sub_comp_95$rf_dist_heavy_n_light), mean(sub_comp_90$rf_dist_heavy_n_light), 
           mean(sub_comp_85$rf_dist_heavy_n_light), mean(sub_comp_80$rf_dist_heavy_n_light), mean(sub_comp_70$rf_dist_heavy_n_light),
           mean(sub_comp_60$rf_dist_heavy_n_light), mean(sub_comp_50$rf_dist_heavy_n_light), mean(sub_comp_40$rf_dist_heavy_n_light),
           mean(sub_comp_30$rf_dist_heavy_n_light), mean(sub_comp_20$rf_dist_heavy_n_light), mean(sub_comp_15$rf_dist_heavy_n_light),
           mean(sub_comp_10$rf_dist_heavy_n_light), mean(sub_comp_5$rf_dist_heavy_n_light), mean(sub_comp_0$rf_dist_heavy_n_light))
df <- data.frame(props = props, values = values, build = build)

write.csv(df, paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/rf_results/sub_build",build, "_", data_source, "_1.csv"))

# bins are 0-10, 11-20, 21-30, 31+
sub_comp_0 <- filter(comp_0, seqs <= 20 & seqs > 10)
sub_comp_5 <- filter(comp_5, seqs <= 20 & seqs > 10)
sub_comp_20 <- filter(comp_20, seqs <= 20 & seqs > 10)
sub_comp_15 <- filter(comp_15, seqs <= 20 & seqs > 10)
sub_comp_20 <- filter(comp_20, seqs <= 20 & seqs > 10)
sub_comp_30 <- filter(comp_30, seqs <= 20 & seqs > 10)
sub_comp_40 <- filter(comp_40, seqs <= 20 & seqs > 10)
sub_comp_50 <- filter(comp_50, seqs <= 20 & seqs > 10)
sub_comp_60 <- filter(comp_60, seqs <= 20 & seqs > 10)
sub_comp_70 <- filter(comp_70, seqs <= 20 & seqs > 10)
sub_comp_80 <- filter(comp_80, seqs <= 20 & seqs > 10)
sub_comp_85 <- filter(comp_85, seqs <= 20 & seqs > 10)
sub_comp_90 <- filter(comp_90, seqs <= 20 & seqs > 10)
sub_comp_95 <- filter(comp_95, seqs <= 20 & seqs > 10)

props <- c(0, 5,10,15,20,30,40,50,60,70,80,85,90,95,100)
values <- c(mean(sub_comp_0$rf_dist_heavy), mean(sub_comp_95$rf_dist_heavy_n_light), mean(sub_comp_90$rf_dist_heavy_n_light), 
            mean(sub_comp_85$rf_dist_heavy_n_light), mean(sub_comp_80$rf_dist_heavy_n_light), mean(sub_comp_70$rf_dist_heavy_n_light),
            mean(sub_comp_60$rf_dist_heavy_n_light), mean(sub_comp_50$rf_dist_heavy_n_light), mean(sub_comp_40$rf_dist_heavy_n_light),
            mean(sub_comp_30$rf_dist_heavy_n_light), mean(sub_comp_20$rf_dist_heavy_n_light), mean(sub_comp_15$rf_dist_heavy_n_light),
            mean(sub_comp_10$rf_dist_heavy_n_light), mean(sub_comp_5$rf_dist_heavy_n_light), mean(sub_comp_0$rf_dist_heavy_n_light))
df <- data.frame(props = props, values = values, build = build)

write.csv(df, paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/rf_results/sub_build",build, "_", data_source, "_2.csv"))

# bins are 0-10, 11-20, 21-30, 31+
sub_comp_0 <- filter(comp_0, seqs <= 30 & seqs > 20)
sub_comp_5 <- filter(comp_5, seqs <= 30 & seqs > 20)
sub_comp_30 <- filter(comp_30, seqs <= 30 & seqs > 20)
sub_comp_15 <- filter(comp_15, seqs <= 30 & seqs > 20)
sub_comp_30 <- filter(comp_30, seqs <= 30 & seqs > 20)
sub_comp_30 <- filter(comp_30, seqs <= 30 & seqs > 20)
sub_comp_40 <- filter(comp_40, seqs <= 30 & seqs > 20)
sub_comp_50 <- filter(comp_50, seqs <= 30 & seqs > 20)
sub_comp_60 <- filter(comp_60, seqs <= 30 & seqs > 20)
sub_comp_70 <- filter(comp_70, seqs <= 30 & seqs > 20)
sub_comp_80 <- filter(comp_80, seqs <= 30 & seqs > 20)
sub_comp_85 <- filter(comp_85, seqs <= 30 & seqs > 20)
sub_comp_90 <- filter(comp_90, seqs <= 30 & seqs > 20)
sub_comp_95 <- filter(comp_95, seqs <= 30 & seqs > 20)

props <- c(0, 5,10,15,20,30,40,50,60,70,80,85,90,95,100)
values <- c(mean(sub_comp_0$rf_dist_heavy), mean(sub_comp_95$rf_dist_heavy_n_light), mean(sub_comp_90$rf_dist_heavy_n_light), 
            mean(sub_comp_85$rf_dist_heavy_n_light), mean(sub_comp_80$rf_dist_heavy_n_light), mean(sub_comp_70$rf_dist_heavy_n_light),
            mean(sub_comp_60$rf_dist_heavy_n_light), mean(sub_comp_50$rf_dist_heavy_n_light), mean(sub_comp_40$rf_dist_heavy_n_light),
            mean(sub_comp_30$rf_dist_heavy_n_light), mean(sub_comp_20$rf_dist_heavy_n_light), mean(sub_comp_15$rf_dist_heavy_n_light),
            mean(sub_comp_10$rf_dist_heavy_n_light), mean(sub_comp_5$rf_dist_heavy_n_light), mean(sub_comp_0$rf_dist_heavy_n_light))
df <- data.frame(props = props, values = values, build = build)

write.csv(df, paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/rf_results/sub_build",build, "_", data_source, "_3.csv"))

# bins are 0-10, 11-20, 21-30, 31+
sub_comp_0 <- filter(comp_0, seqs > 30)
sub_comp_5 <- filter(comp_5, seqs > 30)
sub_comp_10 <- filter(comp_10, seqs > 30)
sub_comp_15 <- filter(comp_15, seqs > 30)
sub_comp_20 <- filter(comp_20, seqs > 30)
sub_comp_30 <- filter(comp_30, seqs > 30)
sub_comp_40 <- filter(comp_40, seqs > 30)
sub_comp_50 <- filter(comp_50, seqs > 30)
sub_comp_60 <- filter(comp_60, seqs > 30)
sub_comp_70 <- filter(comp_70, seqs > 30)
sub_comp_80 <- filter(comp_80, seqs > 30)
sub_comp_85 <- filter(comp_85, seqs > 30)
sub_comp_90 <- filter(comp_90, seqs > 30)
sub_comp_95 <- filter(comp_95, seqs > 30)

props <- c(0, 5,10,15,20,30,40,50,60,70,80,85,90,95,100)
values <- c(mean(sub_comp_0$rf_dist_heavy), mean(sub_comp_95$rf_dist_heavy_n_light), mean(sub_comp_90$rf_dist_heavy_n_light), 
            mean(sub_comp_85$rf_dist_heavy_n_light), mean(sub_comp_80$rf_dist_heavy_n_light), mean(sub_comp_70$rf_dist_heavy_n_light),
            mean(sub_comp_60$rf_dist_heavy_n_light), mean(sub_comp_50$rf_dist_heavy_n_light), mean(sub_comp_40$rf_dist_heavy_n_light),
            mean(sub_comp_30$rf_dist_heavy_n_light), mean(sub_comp_20$rf_dist_heavy_n_light), mean(sub_comp_15$rf_dist_heavy_n_light),
            mean(sub_comp_10$rf_dist_heavy_n_light), mean(sub_comp_5$rf_dist_heavy_n_light), mean(sub_comp_0$rf_dist_heavy_n_light))
df <- data.frame(props = props, values = values, build = build)

write.csv(df, paste0("/gpfs/gibbs/pi/kleinstein/heavy_light/project/simulations/rf_results/sub_build",build, "_", data_source, "_4.csv"))




#########################################################################################################################################
################################# Now get it #################################################################################
###############################################################################################################################

par(mfrow = c(2,2))
# read in the data for bin 1
read_in_data_and_graph <- function(bin_num){
  pratchet <- read.csv(paste0("~/Downloads/sub_buildpratchet_flu_", bin_num, ".csv"))
  pml <- read.csv(paste0("~/Downloads/sub_buildpml_flu_", bin_num, ".csv"))
  dnapars <- read.csv(paste0("~/Downloads/sub_builddnapars_flu_", bin_num, ".csv"))
  dnaml <- read.csv(paste0("~/Downloads/sub_builddnaml_flu_", bin_num, ".csv"))
  rf_all <- rbind(pratchet, pml,dnaml, dnapars)
  if(bin_num ==4){
    rf_all <- rbind(pratchet, pml,dnaml)
  }
  if(bin_num == 1){
    limit <- paste0("Sequences Between 3 and 10")
  } else if(bin_num == 2){
    limit <- paste0("Sequences Between 11 and 20")
  } else if(bin_num ==3){
    limit <- paste0("Sequences Between 21 and 30")
  } else if(bin_num ==4){
    limit <- paste0("Sequences Greater Than 30")
  }
  rf <- ggplot(rf_all, aes(x=props, y=values))+
    theme_bw() +
    geom_line(aes(color = build), size = 2) +
    geom_point(aes(color=build), size = 7) +
    scale_color_manual(values=c("#6CAEDF", "#00471B", "#F9A01B", "#3E2680")) +
    ylab("RF Distance") +
    xlab("Amount of Light Chain Included") +
    ggtitle(limit) +
    theme(plot.title = element_text(hjust = 0.5, size = 14, face = "bold"),
          axis.text = element_text(size=14))
  return(rf)
}
library(gridExtra)
bin_1 <- read_in_data_and_graph(1)
plot(bin_1)
bin_2 <- read_in_data_and_graph(2)
plot(bin_2)
bin_3 <- read_in_data_and_graph(3)
plot(bin_3)
bin_4 <- read_in_data_and_graph(4)
plot(bin_4)
grid.arrange(bin_1, bin_2, bin_3, bin_4, ncol=2)
