library(alakazam)
library(dowser)
library(ggtree)
library(dplyr)
library(ape)
library(shazam)
library(scoper)
library(gridExtra)
library(devtools)
library(parallel)

# read in the rds files -- light and heavy
fhl <- readRDS("total_fhl.rds")

# CHANGE HERE
scale = 0.5
nproc = 36
proportion_light = 0.5

args = commandArgs(trailingOnly=TRUE)
scale = as.numeric(args[1])
nproc = as.numeric(args[2])

if(scale != 0){
  fhl_index <- which(fhl$scale == scale & fhl$rep == 1)
}else{
  fhl_index <- which(fhl$scale == 0.25 & fhl$rep == 1)
}
fhl_sub <- fhl[fhl_index,]
rm(fhl_index, fh_index)
# CHANGE HERE
bootstrap_amount <- 50

# if scale == 0, replace all light chain sequences with light chain germline
if(scale == 0){
  print("Replacing light chain with no sequence information!")
  for(i in 1:nrow(fhl_sub)){
    light = fhl_sub$data[[i]]@locus != "IGH"
    gl = strsplit(fhl_sub$data[[i]]@hlgermline,split="")[[1]][light]
    fhl_sub$data[[i]]@data$hlsequence = 
      unlist(lapply(strsplit(fhl_sub$data[[i]]@data$hlsequence,split=""), 
        function(x){paste0(c(x[!light],gl), collapse="")}))

      l = sum(unlist(lapply(strsplit(fhl_sub$data[[i]]@data$hlsequence,split=""),function(x)
        sum(gl != x[light]))))
      h = sum(unlist(lapply(strsplit(fhl_sub$data[[i]]@data$hlsequence,split=""),function(x)
        sum(strsplit(fhl_sub$data[[i]]@hlgermline,split="")[[1]][!light] != x[!light]))))
      d = sum(unlist(lapply(fhl_sub$data[[i]]@data$hlsequence,function(x)
        alakazam::seqDist(x, fhl_sub$data[[i]]@hlgermline))))
      print(paste(l,h,d))
      if(h != d){
          print("!!")
      }
  }
}

# Loop over all sequences in all clone objects
# Randomly decide (based on parameter) whether to mask a sequence's light chain
# if so, replace light chain in hlsequence with Ns



print('Read in all the functions needed')

splitting_func <- function(tree_data, light_db){
  bob <- c()
  for(i in 1: length(tree_data$data[[1]]@data$hlsequence)){
    bob[i] <- paste(strsplit(tree_data$data[[1]]@data$hlsequence,split="")[[i]][light_db], collapse = "")
  }
  return(bob)
}
light_tree <- function(tree_data, light_index){
  tree_data$data[[1]]@hlgermline <- paste(strsplit(tree_data$data[[1]]@hlgermline,split="")[[1]][light_index], collapse = "")
  tree_data$data[[1]]@numbers <- tree_data$data[[1]]@numbers[light_index]
  tree_data$data[[1]]@locus <- tree_data$data[[1]]@locus[light_index]
  tree_data$data[[1]]@region <- tree_data$data[[1]]@region[light_index]
  tree_data$data[[1]]@data$hlsequence <- splitting_func(tree_data, light_index)
  return(tree_data)
}

plot_heavy_light <- function(tree_data, size_num, xlim_value){
  light_index = tree_data$data[[1]]@locus != "IGH"
  heavy_index = tree_data$data[[1]]@locus == "IGH"
  l <- light_tree(tree_data, light_index)
  h <- light_tree(tree_data, heavy_index)
  ltrees = getTrees(l)
  l_plots <- plotTrees(ltrees)
  l_plots <- lapply(l_plots, function(x)x+geom_tiplab(size=size_num) + ggtitle("Light Chain Tree") + xlim(NA, xlim_value))
  htrees = getTrees(h)
  h_plots <- plotTrees(htrees)
  h_plots <- lapply(h_plots, function(x)x+geom_tiplab(size=size_num) + ggtitle("Heavy Chain Tree") + xlim(NA, xlim_value))
  grid.arrange(l_plots[[1]], h_plots[[1]], nrow = 1)
}

plot_heavy_light_notips <- function(tree_data, xlim_value){
  light_index = tree_data$data[[1]]@locus != "IGH"
  heavy_index = tree_data$data[[1]]@locus == "IGH"
  l <- light_tree(tree_data, light_index)
  h <- light_tree(tree_data, heavy_index)
  ltrees = getTrees(l)
  l_plots <- plotTrees(ltrees)
  l_plots <- lapply(l_plots, function(x)x + ggtitle("Light Chain Tree") + xlim(NA, xlim_value))
  htrees = getTrees(h)
  h_plots <- plotTrees(htrees)
  h_plots <- lapply(h_plots, function(x)x + ggtitle("Heavy Chain Tree") + xlim(NA, xlim_value))
  grid.arrange(l_plots[[1]], h_plots[[1]], nrow = 1)
}

make_split_trees <- function(tree_data){
  light_index = tree_data$data[[1]]@locus != "IGH"
  heavy_index = tree_data$data[[1]]@locus == "IGH"
  l <- light_tree(tree_data, light_index)
  h <- light_tree(tree_data, heavy_index)
  ltrees = getTrees(l)
  htrees = getTrees(h)
  branch_length1 <- as.data.frame(unlist(lapply(ltrees$trees, function(x)sum(x$edge.length))))
  branch_length2 <- as.data.frame(unlist(lapply(htrees$trees, function(x)sum(x$edge.length))))
  colnames(branch_length1) <- c("avg_branch_light")
  colnames(branch_length2) <- c("avg_branch_heavy")
  return(as.data.frame(cbind(branch_length1, branch_length2)))
}


#fhl_sub = fhl_sub[77,]
# make the heavy only tibble 
fh_sub <-lapply(1:length(fhl_sub$clone_id), function(x) light_tree(fhl_sub[x,], fhl_sub$data[[x]]@locus == 'IGH'))
fh_sub <- do.call(rbind, fh_sub)
#fh_sub$data = lapply(fh_sub$data, function(x){
#  x@phylo_seq = "sequence"
#  x
#  })


# bootstrap the two of them 
print('reading in the bootstrapping functions')
bootstrapping <- function(starting_tree_list, b_num, nproc, tree_num){
  b = bootstrapTrees(starting_tree_list, bootstraps=b_num, rm_temp=FALSE, nproc = nproc)
  tree = lapply(b$trees, function(x)x[[tree_num]])
  return(tree)
}

full_tree_func <- function(full_tree, tree_num){
  full_tree_df <- splits_func(list(full_tree),1)
  return(full_tree_df)
}

splits_func <- function(tree, b_num){
  # NOTE: ASSUMES subtrees list is indexed by internal node number (seems to be the case)
  splits <- data.frame(found=I(lapply(subtrees(tree[[b_num]]),function(x)x$tip.label)))
  splits$node <- (Ntip(tree[[b_num]]) + 1):(Ntip(tree[[b_num]]) + tree[[b_num]]$Nnode)
  
  # find the difference between tip labels and the tips in 'found'
  full_tips <- as.vector(tree[[b_num]]$tip.label)
  absent <- c()
  for(i in 1:length(splits$found)){
    found <- as.vector(splits$found[[i]])
    not_found <- setdiff(full_tips, found)
    absent[i] <- list(not_found)
  }
  splits$absent <- data.frame(absent=I(absent))
  splits$tree_num <- b_num
  # reorder it to make sense -- tree number, node number, found tips, and absent tips
  splits <- splits[, c(4,2, 1, 3)]
  return(splits)
}

bootstrap_df <- function(tree, b_num){
  splits <- c()
  for(i in 1:length(tree)){
    to_bind <- splits_func(tree,i)
    splits <- rbind(splits, to_bind)
  }
  return(splits)
}

matching_function <- function(full_tree_df, bootstrap_df){
  match_vector <- vector()
  match_vector = unlist(lapply(unique(full_tree_df$node), function(node){
    print(node)
    sub_full_tree_df <- full_tree_df[which(full_tree_df$node==node),]
    matches = unlist(lapply(1:length(bootstrap_df$tree_num), function(x){
      match_test1 <- dplyr::setequal(bootstrap_df$found[[x]], sub_full_tree_df$found[[1]])
      match_test2 <- dplyr::setequal(bootstrap_df$found[[x]], sub_full_tree_df$absent[[1]])
      if(match_test1 == TRUE || match_test2 == TRUE){
        return(1)
      }else{
        return(0)
      }}))
    #match_vector[node] = sum(matches)
    sum(matches)
  }))
  match_vector <- match_vector[!is.na(match_vector)]
  return(match_vector)
}

matching_function_parallel <- function(full_tree_df, bootstrap_df, nproc){
  print("matching")
  match_vector <- vector()
  match_vector = unlist(mclapply(unique(full_tree_df$node), function(node){
    #print(node)
    sub_full_tree_df <- full_tree_df[which(full_tree_df$node==node),]
    matches = unlist(lapply(1:length(bootstrap_df$tree_num), function(x){
      match_test1 <- dplyr::setequal(bootstrap_df$found[[x]], sub_full_tree_df$found[[1]])
      match_test2 <- dplyr::setequal(bootstrap_df$found[[x]], sub_full_tree_df$absent[[1]])
      if(match_test1 == TRUE || match_test2 == TRUE){
        return(1)
      }else{
        return(0)
      }}))
    sum(matches)
  },mc.cores=nproc))
  match_vector <- match_vector[!is.na(match_vector)]
  return(match_vector)
}

bootstrapping_full <- function(starting_tree_list, full_tree, b_num, nproc, tree_num){
  b_tree <- bootstrapping(starting_tree_list, b_num, nproc, tree_num)
  full_tree <- full_tree_func(full_tree, tree_num)
  bootstraps_df <- bootstrap_df(b_tree, b_num)
  results <- matching_function_parallel(full_tree, bootstraps_df,nproc)
  return(results)
}
print('functions read in')

print('starting the boostrapping -- this may take awhile')
# do bootstrapping for all clones within hlcones and hclones
print(paste0('bootstrap amount is ', bootstrap_amount))
print('starting the hclones bootstrapping')
hclones_avg <- c()
pdf(paste0("plots_sim_hclones_",scale,".pdf"))
for (i in 1:length(fh_sub$clone_id)) {
  full_tree <- getTrees(fh_sub[i,])$trees[[1]]
  full_tree$bootstrap <- bootstrapping_full(starting_tree_list = fh_sub[i,], full_tree = full_tree, b_num = bootstrap_amount, nproc = nproc, tree_num = 1)
  plot(full_tree)
  nodelabels(full_tree$bootstrap)
  tobind <- mean(full_tree$bootstrap)
  hclones_avg <- rbind(hclones_avg, tobind)
  print(paste0("Finished ", i, " of ", length(fh_sub$clone_id)))
}
dev.off()
print('hclones plots complete')
hclones_avg <- as.data.frame(hclones_avg)
write.csv(hclones_avg, file = paste0("hclones_sim_avg_",scale,".csv"))
print('hclones bootstrapping csv file written')
# get rid of now unused variables 
print('hclone plots completed and average bootstrap score found')

print('starting the hlclones bootstrapping')
hlclones_avg <- c()
pdf(paste0("plots_sim_hlclones_",scale,".pdf"))
for (i in 1:length(fhl_sub$clone_id)) {
  full_tree <- getTrees(fhl_sub[i,])$trees[[1]]
  full_tree$bootstrap <- bootstrapping_full(starting_tree_list = fhl_sub[i,], full_tree = full_tree, b_num = bootstrap_amount, nproc = nproc, tree_num = 1)
  plot(full_tree)
  nodelabels(full_tree$bootstrap)
  tobind <- mean(full_tree$bootstrap)
  hlclones_avg <- rbind(hlclones_avg, tobind)
  print(paste0("Finished ", i, " of ", length(fhl_sub$clone_id)))
}
dev.off()
print('hlclones plots complete')
hlclones_avg <- as.data.frame(hlclones_avg)
write.csv(hlclones_avg, file = paste0("hlclones_sim_avg_",scale,".csv"))
print('hlclones bootstrapping csv file written')
print('hclone plots completed and average bootstrap score found')


hclones_avg <- read.csv(paste0("hclones_sim_avg_",scale,".csv"))
hlclones_avg <- read.csv(paste0("hlclones_sim_avg_",scale,".csv"))


hclones_avg$X <- 1:length(hclones_avg[,1])
colnames(hclones_avg) <- c("tree_num", "average_hclones")
hlclones_avg$X <- 1:length(hlclones_avg[,1])
colnames(hlclones_avg) <- c("tree_num", "average_hlclones")

# make sure they're dfs 
hclones_avg <- as.data.frame(hclones_avg)
hlclones_avg <- as.data.frame(hlclones_avg)

comp <- merge(hclones_avg, hlclones_avg)

for(i in 1:length(hclones_avg$tree_num)){
  if((comp$average_hclones[i] > comp$average_hlclones[i]) == T){
    comp$larger_value[i] <- "hclones"
    comp$l_value[i] <- comp$average_hclones[i]
  }
  else if((comp$average_hlclones[i] > comp$average_hclones[i]) == T){
    comp$larger_value[i] <- "hlclones"
    comp$l_value[i] <- comp$average_hlclones[i]
  }
  else{
    comp$larger_value[i] <- "same"
    comp$l_value[i] <- comp$average_hclones[i]
  }
}


jpeg(paste0("table_",scale,".jpeg"), type="cairo")
p <- tableGrob(as.data.frame(table(comp$larger_value)))
grid.arrange(p)
dev.off()

library(ggplot2)
jpeg(file = paste0("Larger of three_",scale,".jpeg"), type="cairo", height = 1000, width = 1000)
legendtitle <- "Larger Clone Type"
avg_overall <- ggplot(comp, aes(x=tree_num, y = l_value, color = larger_value)) + geom_point(size = 5)
avg_overall + scale_color_manual(legendtitle, values = c("#627e8d", "#88afa6", "#d49421")) + ggtitle('Average Bootstrap Value by Tree') +
  xlab("Tree Number") + ylab("Average Bootstrap Value") +theme(plot.title = element_text(hjust = 0.5))
dev.off()



test <- comp[c(-3,-4,-5)]
test$type <- "hclone"
colnames(test) <- c("tree_num", "average", "type")


test1 <- comp[c(-2,-4,-5)]
test1$type <- "hlclone"
colnames(test1) <- c("tree_num", "average", "type")

test_full <- rbind(test, test1)
jpeg(paste0("hlclones_vs_hlcones_",scale,".jpeg"), type="cairo", height = 1000, width = 1000)
legendtitle2 <- "Clone Type"
comparison <- ggplot(test_full, aes(x=tree_num, y = average, color = type)) +
  geom_point(size = 5) +
  ggtitle('Average Bootstrap Value by Tree') +
  xlab("Tree Number") + ylab("Average Bootstrap Value") +
  theme(plot.title = element_text(hjust = 0.5)) + 
  scale_color_manual(legendtitle2, values = c("#627e8d", "#88afa6")) + 
  theme(
    panel.background = element_rect(fill = "transparent"), # bg of the panel
    plot.background = element_rect(fill = "transparent", color = NA), # bg of the plot
    panel.grid.major = element_blank(), # get rid of major grid
    panel.grid.minor = element_blank(), # get rid of minor grid
    legend.background = element_rect(fill = "transparent"), # get rid of legend bg
    legend.box.background = element_rect(fill = "transparent")) # get rid of legend panel bg) #+ 
comparison
dev.off()
# get the clone sizes
comp$seq_size <- fhl_sub$seqs

jpeg(file=paste0("text_plot_covid_",scale,".jpeg"), type="cairo", height = 1000, width = 1000)
test <- ggplot(comp, aes(x=average_hclones, y = average_hlclones)) + 
  geom_text(aes(label=seq_size, size = 5)) + 
  theme_bw() +
  ggtitle('Average Bootstrap Value of Hlclones by Hclones') +
  xlab("hclones") + ylab("hlcones") +
  theme(plot.title = element_text(hjust = 0.5)) + 
  geom_abline(slope = 1) 
test
dev.off()

write.csv(comp, file = paste0("comp_",scale,".csv"))
comp = read.csv(file = paste0("comp_",scale,".csv"))
# see whats up with clone 33/12

branches <-  lapply(1:length(fhl_sub$clone_id), function(x) make_split_trees(fhl_sub[x,]))
test <- data.frame(branches)
pdf(paste0("sim_comp_with_tips_",scale,".pdf"))
lapply(1:length(fhl_sub$clone_id), function(x) plot_heavy_light(fhl_sub[x,], 2, .35))
dev.off()


# now the ones without tips 
pdf(paste0("sim_comp_with_notips_",scale,".pdf"))
lapply(1:length(fhl_sub$clone_id), function(x) plot_heavy_light_notips(fhl_sub[x,], .35))
dev.off()



#  bob <- getTrees(fhl_sub)
#  bobert <- getTrees(fh_sub)
#  total_branch_length <- as.data.frame(cbind(unlist(lapply(bob$trees, function(x)sum(x$edge.length))), unlist(lapply(bobert$trees, function(x)sum(x$edge.length)))))
#  hydro <- as.data.frame(total_branch_length[,1])
#  flask <- as.data.frame(total_branch_length[,2])
#  colnames(hydro) <- c("total_branch")
#  colnames(flask) <- c("total_branch")
#  total_branch_length <- rbind(hydro, flask)
#  avg_branch_length1 <- as.data.frame(unlist(lapply(bob$trees, function(x)mean(x$edge.length))))
#  avg_branch_length2 <- as.data.frame(unlist(lapply(bobert$trees, function(x)mean(x$edge.length))))
#  colnames(avg_branch_length1) <- c("avg_branch")
#  colnames(avg_branch_length2) <- c("avg_branch")
#  avg_branch_length <- rbind(avg_branch_length1, avg_branch_length2)
#
#  # now do a paired t-test
#  print(paste("T-test to see if there is a difference at all ", t.test(comp$average_hclones, comp$average_hlclones, paired = TRUE, alternative = "two.sided")))
#
#
#  # now lets do it the way the example wants 
#  # see if its different 
#  heavy <- as.data.frame(rep("heavy", length(hlclones_avg$tree_num)))
#  hl <- as.data.frame(rep("heavy_light", length(hlclones_avg$tree_num)))
#  colnames(heavy) <- c("group")
#  colnames(hl) <- c("group")
#  my_data <- rbind(heavy, hl)
#  hbs_scores <- as.data.frame(comp$average_hclones)
#  hlbs_scores <- as.data.frame(comp$average_hlclones)
#  colnames(hbs_scores) <- c("score")
#  colnames(hlbs_scores) <- c("score")
#  bob <- rbind(hbs_scores, hlbs_scores)
#  my_data <- cbind(my_data, bob, total_branch_length,avg_branch_length)
#
#  write.csv(my_data, file = paste0("t-test_",scale,".csv"))
#
#  library("ggpubr")
#  jpeg(paste0("group by score_",scale,".jpeg"), type="cairo")
#  ggboxplot(my_data, x = "group", y = "score", 
#            color = "group", palette = c("#627e8d", "#88afa6"),
#            order = c("heavy", "heavy_light"),
#            ylab = "Average Bootstrap Score", xlab = "Groups")
#  dev.off()
#  jpeg(paste0("group by total branch length_",scale,".jpeg"), type="cairo")
#  ggboxplot(my_data, x = "group", y = "total_branch", 
#            color = "group", palette = c("#627e8d", "#88afa6"),
#            order = c("heavy", "heavy_light"),
#            ylab = "Total Branch Length", xlab = "Groups")
#  dev.off()
#  jpeg(paste0("group by avg_branch length_",scale,".jpeg"), type="cairo")
#  ggboxplot(my_data, x = "group", y = "avg_branch", 
#            color = "group", palette = c("#627e8d", "#88afa6"),
#            order = c("heavy", "heavy_light"),
#            ylab = "Average Branch Length", xlab = "Groups")
#  dev.off()
#  # Subset score data by heavy
#  heavy <- subset(my_data,  group == "heavy", score,
#                  drop = TRUE)
#  # subset score data by heavy light
#  heavy_light <- subset(my_data,  group == "heavy_light", score,
#                        drop = TRUE)
#  # Plot paired data
#  library(PairedData)
#  pd <- paired(heavy, heavy_light)
#  jpeg(paste0("profile of score by groups_",scale,".jpeg"), type="cairo")
#  plot(pd, type = "profile") + theme_bw()
#  dev.off()
#  # subset by group real quick 
#  tot_h <- subset(my_data, group == "heavy", total_branch, drop = T)
#  tot_hl <- subset(my_data, group == "heavy_light", total_branch, drop = T)
#  pd_tot <- paired(tot_h, tot_hl)
#  jpeg(paste0("profile by total branch length by group_",scale,".jpeg"), type="cairo")
#  plot(pd_tot, type = "profile") + theme_bw()
#  dev.off()
#  # one more time yall 
#  avg_h <- subset(my_data, group == "heavy", avg_branch, drop = T)
#  avg_hl <- subset(my_data, group == "heavy_light", avg_branch, drop = T)
#  pd_avg <- paired(avg_h, avg_hl)
#  jpeg(paste0("profile of average branch lengths by group_",scale,".jpeg"), type="cairo")
#  plot(pd_avg, type = "profile") + theme_bw()
#  dev.off()
#  # compute the difference
#  d <- as.data.frame(with(my_data, 
#                          score[group == "heavy"] - score[group == "heavy_light"]))
#  colnames(d) <- "score difference"
#  d1 <- as.data.frame(with(my_data, 
#                           total_branch[group == "heavy"] - total_branch[group == "heavy_light"]))
#  colnames(d1) <- "total difference"
#  d2 <- as.data.frame(with(my_data, 
#                           avg_branch[group == "heavy"] - avg_branch[group == "heavy_light"]))
#  colnames(d2) <- "avg difference"
#  differences <- cbind(d, d1, d2)
#
#  write.csv(differences, file = paste0("differences_",scale,".csv"))
#  print("FINISHED")
