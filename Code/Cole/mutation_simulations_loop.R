
######################################################################################################################
#################################### This is the simulation results loop script ######################################
# I want the script to have the following options
# 1. Masking y/n
# 2. Model options (mutaional rates and model used; basically a numeric input and a y/n)
# 3. Sequence cutoff amount 
# 4. Number of processors used for the task 
# 5. How many iterations of the simulations

# I'll add comments throughout to help shed light on what is actually going on. Typically they will be above the line 
# of code that they are meant to illuminate. Occasionally they will be off to the right side. 


# read in the libraries needed
library(dplyr)
library(dowser)
library(alakazam)
library(scoper)
library(igraph)
library(shazam)
library(phangorn)

# set the workig directory to ensure that you're saving the data where you want it
setwd("~/project/PhD/Kleinstein/heavy_light/project/simulations")


# read in the command line inputs/options 
args = commandArgs(trailingOnly=TRUE)
mutational_rate = as.numeric(args[1])
lc_model = as.name(args[2])
cut_off = as.numeric(args[3])
nprocs = as.numeric(args[4])
niter = as.numeric(args[5])


# read in the starting data
print("read in the data")
original_trees = readRDS("~/project/PhD/Kleinstein/heavy_light/project/simulations/datastates_cluster_all_genotyped.rds")
#original_trees <- readRDS("datastates_cluster_all_genotyped.rds")
naive_heavy_chain <- readChangeoDb("~/project/PhD/Kleinstein/heavy_light/project/simulations/naive_heavy_chain.tsv")
#naive_heavy_chain <- readChangeoDb("naive_heavy_chain.tsv")
naive_heavy_chain <- filter(naive_heavy_chain, !grepl("-", sequence_alignment))
naive_light_chain <- readChangeoDb("~/project/PhD/Kleinstein/heavy_light/project/simulations/naive_light_chain.tsv")
#naive_light_chain <- readChangeoDb("naive_light_chain.tsv")
naive_light_chain <- filter(naive_light_chain, !grepl("-", sequence_alignment))
print("data successfully read in")

# create the objects to save to when going through the loop
full_results <- c()
# start the loop
for (iter in 1:niter) {
  print(paste0("starting iteration ",iter, " of ", niter))
  # now build some trees
  print("scaling the trees")
  test <- filter(original_trees, seqs > cut_off)
  original_trees_filtered <- scaleBranches(test)
  original_trees <- scaleBranches(original_trees) #sets it so that the tress branches are scaled consistent with igraph obj
  
  print("making graphs")
  graphs = list()
  for (i in 1:nrow(original_trees_filtered)){
    #tree = original_trees_mod$trees[[i]]
    original_trees_filtered$trees[[i]]$edge.length = round(original_trees_filtered$trees[[i]]$edge.length) + 1
    graphs[[i]] <- phyloToGraph(original_trees_filtered$trees[[i]]) #puts the trees into Igraph format
  }
  
  graphs_half = list()
  for (i in 1:nrow(original_trees_filtered)){
    #tree = original_trees_mod$trees[[i]]
    original_trees_filtered$trees[[i]]$edge.length = ceiling(original_trees_filtered$trees[[i]]$edge.length/mutational_rate)
    graphs_half[[i]] <- phyloToGraph(original_trees_filtered$trees[[i]]) #puts the trees into Igraph format
  }
  
  graphs[sapply(graphs, is.null)] <- NULL
  print("graphs complete")
  
  
  # Filter out so that only the new_cell_ids that are in the heavy and light chain data are included in the set
  print("filter the chains so that there are no chains with gaps")
  naive_cell_id_set <- filter(naive_light_chain, new_cell_id %in% naive_heavy_chain$new_cell_id)
  naive_cell_id_set <- naive_cell_id_set$new_cell_id
  naive_cell_id_set <- unique(naive_cell_id_set)
  print("filtering complete")
  
  # for each tree, select random heavy and light chain, simulate, process
  # get each from a naive b cell from the combined dataset
  print("mutate the trees -- create FRANKENSTEIN")
  mutated_tree_list = list()
  
  # create Frankenstein database using the mutated_list
  
  frankenstein_heavy_db <- data.frame(matrix(ncol=ncol(naive_heavy_chain), nrow=0))
  frankenstein_light_db <- data.frame(matrix(ncol=ncol(naive_light_chain), nrow=0))
  colnames(frankenstein_heavy_db) <- colnames(naive_heavy_chain)
  colnames(frankenstein_light_db) <- colnames(naive_light_chain)
  
  for (ii in 1:length(graphs)) {
    phylo_tree = graphs[[ii]]
    half_tree = graphs_half[[ii]]
    random_id = sample(naive_cell_id_set, 1) #gets random new_cell_id
    temp_seq_h = naive_heavy_chain[naive_heavy_chain$new_cell_id == random_id,][1,] #gets random heavy chain using id
    temp_seq_l = naive_light_chain[naive_light_chain$new_cell_id == random_id,][1,] #gets random light chain using id
    temp_sim_tree_h = shmulateTree(temp_seq_h$sequence_alignment, phylo_tree, targetingModel = HH_S5F)            #simulate the  heavy chain mutations
    # simulate the light chain mutaitons based on what model input is used
    if(lc_model == "HH_S5F"){
      temp_sim_tree_l = shmulateTree(temp_seq_l$sequence_alignment, half_tree, targetingModel = HH_S5F)
    } else{
      temp_sim_tree_l = shmulateTree(temp_seq_l$sequence_alignment, half_tree, targetingModel = HKL_S5F)           
    }
    
    
    mutated_tree_list[[random_id]] = c(temp_sim_tree_h, temp_sim_tree_l) #store the simulated trees just in case
    
    #make an entry in the new database for each mutated sequence
    for (ind in 1:length(temp_sim_tree_h$sequence)){
      if (nchar(temp_sim_tree_h$name[ind]) > 6){ #as long as inferred isn't in the sequence
        temp_heavy_row = temp_seq_h[rep(1,1), ] # make copy of heavy row
        temp_light_row = temp_seq_l[rep(1,1), ] # make copy of light row
        
        temp_seq_align = temp_heavy_row$sequence_alignment
        temp_heavy_row$sequence_alignment = temp_sim_tree_h$sequence[ind]     # mutated sequence
        temp_heavy_row$germline_alignment = temp_seq_align                    # original sequence
        temp_heavy_row$sequence_id = temp_sim_tree_h$name[ind]                # sequence_id taken from tree
        temp_heavy_row$clone_id = original_trees_filtered$clone_id[ii]                # clone_id taken from original tree
        
        tmp_str = sapply(strsplit(temp_sim_tree_h$name[ind], "-"), "[", 1)
        tmp_cell_id = paste(tmp_str, temp_heavy_row$clone_id, sep="-")
        
        temp_heavy_row$cell_id = tmp_cell_id                                  # cell_id = first part of seq_id plus clone number
        frankenstein_heavy_db <- rbind(frankenstein_heavy_db, temp_heavy_row) # add the temporary row to the data frame
        
        temp_seq_align = temp_light_row$sequence_alignment
        temp_light_row$sequence_alignment = temp_sim_tree_l$sequence[ind]     # mutated sequence
        temp_light_row$germline_alignment = temp_seq_align                    # original sequence
        temp_light_row$sequence_id = temp_sim_tree_l$name[ind]                # sequence_id taken from tree
        temp_light_row$cell_id = tmp_cell_id                                  # cell_id same as heavy chain
        temp_light_row$clone_id = original_trees_filtered$clone_id[ii]                # clone_id taken from original tree
        frankenstein_light_db <- rbind(frankenstein_light_db, temp_light_row) # add the temporary row to the data frame
      }
    }
  }
  
  print("the mutated trees have been made")
  print("frankenstein heavy and light dbs created")
  #rm(temp_heavy_row, temp_light_row, temp_seq_h, temp_seq_l, temp_sim_tree_h, temp_sim_tree_l, graphs, graphs_half, half_tree, 
  #  mutated_tree_list, naive_heavy_chain, naive_light_chain, original_trees, original_trees_filtered, phylo_tree, i, ii, ind, 
  # naive_cell_id_set,random_id, temp_seq_align, tmp_cell_id,tmp_str)
  
  # remove the cells with multiple heavy chains 
  heavy_count = table(frankenstein_heavy_db$cell_id)
  sum(heavy_count > 1)
  multi_heavy_cells = names(heavy_count[heavy_count > 1])
  frankenstein_heavy_db = filter(frankenstein_heavy_db, !cell_id %in% multi_heavy_cells)
  rm(heavy_count, multi_heavy_cells)
  
  print("getting sub clones and combining heavy and light chain data")
  comb = getSubclones(frankenstein_heavy_db, frankenstein_light_db, cell="cell_id", nproc=nprocs)
  
  # make clone ids unique
  comb$clone_id = paste0(comb$clone_id, "-", comb$subclone_id)
  
  comb = comb[which(!is.na(comb$c_call)),]
  print("combining complete")
  
  print("formatting clones")
  hlclones = formatClones(comb,chain="HL",
                          split_light=TRUE,heavy="IGH",cell="cell_id",
                          minseq = 2, collapse = FALSE, nproc=nprocs, germ = "germline_alignment")
  
  hclones = formatClones(comb,chain="H",
                         split_light=TRUE,heavy="IGH",cell="cell_id",
                         minseq = 2, collapse = FALSE, nproc=nprocs, germ = "germline_alignment")
  print("formatting complete")
  
  print("making trees")
  heavy_trees <- getTrees(hclones, nproc = nprocs)
  light_n_heavy_trees <- getTrees(hlclones, nproc = nprocs)
  print("trees created")
  
  # Match operation for the trees
  print("matching operation started and some data sanity checks being done")
  m_h = match(paste0(original_trees_filtered$clone_id, "-1_1"), heavy_trees$clone_id)
  m_l = match(paste0(original_trees_filtered$clone_id, "-1_1"), light_n_heavy_trees$clone_id)
  t_h = match(heavy_trees$clone_id, paste0(original_trees_filtered$clone_id, "-1_1"))
  t_l = match(light_n_heavy_trees$clone_id, paste0(original_trees_filtered$clone_id, "-1_1"))
  
  # check to make sure that there are no NA values in m
  if ((sum(is.na(m_h)) > 0) || (sum(is.na(m_l)) > 0)){
    stop("THERE ARE NA VALUES IN THE MATCH ARRAY")
  }
  
  print("creating results file")
  
  heavy_trees_new = heavy_trees[m_h,]
  light_n_heavy_trees_new = light_n_heavy_trees[m_l,]
  
  rf_dist_list_heavy = list()
  results = tibble()
  
  for (i in 1:nrow(heavy_trees)){
    rf_dist_heavy <- RF.dist(original_trees_filtered$trees[[i]], heavy_trees_new$trees[[i]]) #original, mutated
    rf_dist_heavy_n_light <- RF.dist(original_trees_filtered$trees[[i]], light_n_heavy_trees_new$trees[[i]])
    temp = tibble(clone_id = original_trees$clone_id[i], 
                  rf_dist_heavy = rf_dist_heavy, 
                  rf_dist_heavy_n_light = rf_dist_heavy_n_light,
                  seqs = original_trees$seqs[i])
    results = bind_rows(results, temp)
  }
  full_results <- rbind(full_results, results)
  print(paste0("Completed simulation iteration ", iter, " of ", niter))
  print("")
  print("")
  print("")
  print("")
  print("")
}


print('Getting the average results and saving both the average and full results')
# get the average values 
avg_results <- c()
for(clone in unique(full_results$clone_id)){
  temp <- filter(full_results, clone_id == clone)
  to_bind <- data.frame(clone_id = as.numeric(clone), avg_rf_dist_heavy = mean(temp$rf_dist_heavy),
                        avg_rf_dist_heavy_n_light = mean(temp$rf_dist_heavy_n_light), seqs = mean(temp$seqs))
  avg_results <- rbind(avg_results, to_bind)
}


# save the results 
saveRDS(full_results, "full_results.rds")
saveRDS(avg_results, "avg_results.rds")
print("results file created")

# Plot the data from the results 
print("making plots")
pdf("h_by_hl.pdf", width = 4, height = 3)
ggplot(avg_results, aes(x = avg_rf_dist_heavy, y = avg_rf_dist_heavy_n_light, size = seqs)) + geom_point() + geom_abline()+
  xlab("Average Heavy only Distance") + ylab("Average Heavy and Light Distance") + ggtitle("Comparing the Average Robinson Fold Distance") +
  theme(plot.title = element_text(hjust = 0.5))
dev.off()

# Compare heavy chain tree and heavy + light chain tree
pdf("boxplots.pdf", width = 4, height = 3)
boxplot(avg_results$avg_rf_dist_heavy_n_light, avg_results$avg_rf_dist_heavy,
        main = "Heavy and Light Chain RF Distance vs Heavy Chain RF Distance",
        ylab = "Robinson Fold Distance",
        names = c("Heavy and Light", "Heavy Only"))

dev.off()
#print("plots created")
print("SIMULATIONS FINISHED")