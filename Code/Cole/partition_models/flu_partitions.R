###################################### Create the Partition Model for the Flu Data ########################################################
library(dplyr)
library(dowser)
library(alakazam)
library(igraph)
library(shazam)

# set the working directory to ensure you're pulling and pushing the right data.
#setwd("~/project/PhD/Kleinstein/heavy_light/project/partition_models/flu")
setwd("~/project/PhD/Kleinstein/heavy_light/project/partition_models/flu")
# identify the command level inputs 
args = commandArgs(trailingOnly=TRUE)
masked_decision = as.logical(args[1])
nprocs = as.numeric(args[2])
secondary = as.logical(args[3])

# this is the test to see if the masked_decision works. Delete after completion
#masked_decision = TRUE
#nprocs = 1
#secondary = FALSE
# pull the data 
print("read in the data")
heavy <- readChangeoDb("heavy.tsv")
heavy <- filter(heavy, !grepl("-", sequence_alignment))
light <- readChangeoDb("light.tsv")
light <- filter(light, !grepl("-", sequence_alignment))
print("data successfully read in")

# remove the cells with multiple heavy chains 
heavy_count = table(heavy$cell_id)
sum(heavy_count > 1)
multi_heavy_cells = names(heavy_count[heavy_count > 1])
heavy = filter(heavy, !cell_id %in% multi_heavy_cells)
rm(heavy_count, multi_heavy_cells)

# set up the naming conventions
heavy_name <- "heavy.rds"
light_name <- "light.rds"
comb_name <- "comb_unmasked.rds"
if(masked_decision == TRUE){
  heavy_name <- "heavy_masked.rds"
  light_name <- "light_masked.rds"
  comb_name <- "comb_masked.rds"
}
# save the files 
saveRDS(heavy, heavy_name)
saveRDS(light, light_name)

print("create the combined file")
comb = getSubclones(heavy,light,cell="cell_id", nproc= nprocs)
# remove the entries where c_call is missing
comb = comb[which(!is.na(comb$c_call)),]
# save this thing since it's acting funky
saveRDS(comb, comb_name)

print("create the germlines")
#references = readIMGT(dir = "~/share/germlines/imgt/human/vdj")
references = readIMGT(dir = "/gpfs/ysm/project/kleinstein/cj448/PhD/Kleinstein/imgt/human/vdj")
h = createGermlines(filter(comb,locus=="IGH"), clone = "clone_id", references, nproc= nprocs)
k = createGermlines(filter(comb,locus=="IGK"), clone = "clone_id", references,locus="IGK", nproc= nprocs)
l = createGermlines(filter(comb,locus=="IGL"), clone = "clone_id", references,locus="IGL", nproc= nprocs)

comb_germline = bind_rows(h, l, k)

# if you want to mask the data uncomment this section 
if(masked_decision == TRUE){
  print("masking the sequence alignments")
  comb_germline$sequence_id <- paste0(substr(comb_germline$sequence_alignment, 1, 7), "_", comb_germline$sequence_id)
  comb_germline <- maskSequences(comb_germline, sequence_id = "sequence_id", sequence = "sequence", sequence_alignment = "sequence_alignment", nproc = nprocs)
  print("masking complete")
}


# figure out what clones to exclude pa more
print("find the clones to exclude")
exclude = comb_germline %>%
  filter(locus != "IGH") %>%
  group_by(clone_id, subclone_id) %>%
  summarize(njunction = n_distinct(junction_length)) %>%
  filter(njunction > 1) %>%
  .$clone_id

# now format it 
print("formatting nonexlcuded clones")
fl = formatClones(filter(comb_germline,!clone_id %in% exclude), majoronly=TRUE,verbose=FALSE,
                  heavy="IGH", chain="HL", cell="cell_id", minseq=2,
                  columns = c("sample"), dup_singles=TRUE, nproc = nprocs, 
                  germ = "germline_alignment_d_mask")
print("formatting clones completed")

# at this point you have to run it on farnam otherwise your RAM will kill the session. 
temp_dir <- "temp"
if(masked_decision == TRUE){
  temp_dir <- "temp_masked"
}
results = list()
igphyml_exec <- "/gpfs/ysm/project/kleinstein/cj448/PhD/Kleinstein/igphyml/src/igphyml"
if(secondary == TRUE){
  igphyml_exec <- "/gpfs/ysm/project/kleinstein/cj448/PhD/Kleinstein/heavy_light/project/partition_models/real_data/unmasked/igphyml/src/igphyml"
}
for(f in unique(fl$sample)){
  print(f)
  temp = filter(fl, sample == f)
  
  print(paste0("starting hl for ", f))
  hl = getTrees(temp, build="igphyml", partition="single", dir=temp_dir,id="single",
                exec=igphyml_exec, nproc=nprocs)
  print(paste0("finished hl for ", f))
  
  print(paste0("starting hl_cf for ", f))
  hl_cf = getTrees(temp, build="igphyml", partition="cf", dir=temp_dir,id="cf",
                   exec=igphyml_exec,nproc=nprocs)
  print(paste0("finished hl_cf for ", f))
  
  print(paste0("starting hl_hl for ", f))
  hl_hl = getTrees(temp, build="igphyml", partition="hl", dir=temp_dir,id="hl",
                   exec=igphyml_exec,nproc=nprocs)
  print(paste0("finished hl_hl for ", f))
  
  print(paste0("starting hl_hlcf for ", f))
  hl_hlcf = getTrees(temp, build="igphyml", partition="hlcf", dir=temp_dir,id="hlcf",
                     exec=igphyml_exec,nproc=nprocs)
  print(paste0("finished hl_hlcf for ", f))
  
  print("creating the results file")
  results[[f]] = list(single = hl, cf = hl_cf, hl=hl_hl, hlcf=hl_hlcf)
}


# set up more names
results_name <- "results_list_unmasked.rds"
lhoods_name <- "lhoods_unmasked.rds"
omegas_name <- "omegas_unmasked.rds"
pic_nam <- "lratio_tests_unmasked.pdf"
if(masked_decision == TRUE){
  results_name <- "results_list_masked.rds"
  lhoods_name <- "lhoods_unmasked.rds"
  omegas_name <- "omegas_masked.rds"
  pic_nam <- "lratio_tests_masked.pdf"
}

saveRDS(results, results_name)

lhoods = list()
for (subjects in names(results)) {
  temp_sub <- results[[subjects]]
  lh <- tibble()
  for (model in names(temp_sub)) {
    df <- 1
    if(model == "hlcf"){
      df = 3
    }
    temp_mod <- temp_sub[[model]]
    lhood <- sum(unlist(lapply(temp_mod$parameters, function(x)x$lhood)))
    to_bind <- tibble(subject = subjects, partition = model, lhood = lhood, df = df, nclone = nrow(temp_mod))
    lh <- bind_rows(lh, to_bind)
  }
  lh$relative_lhood = lh$lhood - filter(lh, partition=="single")$lhood
  lh$pvalue = 1 - pchisq(2*lh$relative_lhood, df=lh$df)
  lhoods = bind_rows(lhoods, lh)
  
}

omegas = list()
for(n in names(results)){
  temp = results[[n]]
  lh = tibble()
  params = unlist(lapply(temp, 
                         function(x)x$parameters[[1]][grepl("omega",names(x$parameters[[1]]))]))
  
  model = unlist(lapply(strsplit(names(params),split="\\."),function(x)x[1]))
  param = unlist(lapply(strsplit(names(params),split="_"),function(x)x[2]))
  value = params
  omegas = bind_rows(omegas,tibble(subject=n, partition=model, param=param, value=value))
}

lhoods$partition[lhoods$partition == "single"] = "Single"
lhoods$partition[lhoods$partition == "cf"] = "CDR/FWR"
lhoods$partition[lhoods$partition == "hl"] = "Heavy/Light"
lhoods$partition[lhoods$partition == "hlcf"] = "Heavy/Light+CDR/FWR"

omegas$partition[omegas$partition == "single"] = "Single"
omegas$partition[omegas$partition == "cf"] = "CDR/FWR"
omegas$partition[omegas$partition == "hl"] = "Heavy/Light"
omegas$partition[omegas$partition == "hlcf"] = "Heavy/Light+CDR/FWR"

saveRDS(lhoods, lhoods_name)
saveRDS(omegas, omegas_name)

pdf(pic_nam, width=8,height=4)
ggplot(lhoods, aes(x=subject,y=relative_lhood,fill=partition,
                   size=nclone))+geom_point(pch=21)+
  theme_bw()+
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))+
  ylab("Likelihood ratio against single partition")+
  xlab("Subject name")+
  labs(size="Clones",fill="Partition Model")

ggplot(lhoods, aes(x=subject,y=-log(pvalue),fill=partition,
                   size=nclone))+geom_point(pch=21)+
  theme_bw()+
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))+
  geom_hline(yintercept=-log(0.05),linetype="dashed",color="red")+
  ylab("-log(p value) against single partition")+
  xlab("Subject name")+
  labs(size="Clones",fill="Partition Model")

ggplot(omegas, aes(x=subject,y=value,fill=param))+geom_point(pch=21,size=2)+
  facet_wrap(.~partition, scales="free")+
  theme_bw()+
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))+
  ylab("Value")+
  xlab("Subject name")+
  labs(fill="Parameter")+ylim(0,2)+
  scale_fill_brewer(palette="Paired")
dev.off()

print("IT FINISHED")
