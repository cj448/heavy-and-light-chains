library(dplyr)
library(dowser)
library(alakazam)

setwd("~/project/PhD/Kleinstein/heavy_light/project/partition_models")
print("read in the data")
heavy = readChangeoDb("data/all_cloned_data.tsv")
light = readChangeoDb("data/light_chain.tsv")

heavy$clone_id = paste0(heavy$subject,"_",heavy$clone_id)
heavy$sequence_id = paste0(heavy$sequence_id,1:nrow(heavy))
light$sequence_id = paste0(light$sequence_id,1:nrow(light))

heavy$subject = as.character(heavy$subject)
light$subject = as.character(light$subject)
light$subject = gsub("_B","",light$sample)

print("create the combined file")
comb = getSubclones(heavy,light,cell="new_cell_id", nproc=20)

print("create the germlines")
references = readIMGT(dir = "/gpfs/ysm/project/kleinstein/cj448/PhD/Kleinstein/imgt/human/vdj")
h = createGermlines(filter(comb,locus=="IGH"),references, nproc=20)
k = createGermlines(filter(comb,locus=="IGK"),references,locus="IGK", nproc=20)
l = createGermlines(filter(comb,locus=="IGL"),references,locus="IGL", nproc=20)

comb_germline = bind_rows(h, l, k)

print("find the covid categories")
# subjects within the germline are 2010113866, 2010113873, 2010113875, 2010113904, 201657034,  
# 201657036, 201657037, 201657083, 201657785, 201657787, 201657790, TP9B, TS2B, TS4B
covid = c("201657035","201657036","201657037","201657783",
  "201657034","201657785","201657787","201657790") 
# 1st and 4th are not in the subjects of the comb_germline 
healthy = c("2010113903", "2010113866", "2010113873", 
  "2010113875", "2010113904")
# the 1st are not in the subjects of the comb_germline
covid = c(covid,"201657083")
severe = c("TP9B", "TS2B", "TS4B")

comb_germline$condition = "Not found"
comb_germline[comb_germline$subject %in% covid,]$condition = "Mild"
comb_germline[comb_germline$subject %in% healthy,]$condition = "Healthy"
comb_germline[comb_germline$subject %in% severe,]$condition = "Severe"

print("ensure that the there is only one per new_cell_id")
max(table(filter(comb_germline, locus=="IGH")$new_cell_id))

# nothing looks weird, but filter out the weird clone to see if it's just that one
#comb_germline_filtered <- comb_germline %>%
 #                           filter(!clone_id %in% '2010113875_14378_6365')

# (note from Ken to future Ken) 
# Change getSubclones to distinguish light chain V, J, AND junction length groups

print("save the preformatted clones")
saveRDS(comb_germline, "masked/full_preclones.rds")

print("find the clones to exclude")
exclude = comb_germline %>%
    filter(locus != "IGH") %>%
    group_by(clone_id, subclone_id) %>%
    summarize(njunction = n_distinct(junction_length)) %>%
    filter(njunction > 1) %>%
    .$clone_id

print("remove the bad clones")
# do this to avoid the igphyml can't deal with stop codons error
comb_germline <- filter(comb_germline, !grepl("-", sequence_alignment))
saveRDS(filter(comb_germline, !clone_id %in% exclude), "masked/preclones.rds")
print("preclones and full_preclones created")

# mask the sequence_alignments
print("masking the sequence alignments")
comb_germline <- maskSequences(comb_germline, sequence_id = "sequence_id", sequence = "sequence", sequence_alignment = "sequence_alignment", nproc = 20)
print("masking complete")

# get rid of clones that don't have the needed information after masking 
bad_info <- filter(comb_germline, is.na(comb_germline$sequence_masked))$clone_id
comb_germline <- filter(comb_germline, !clone_id %in% bad_info)
# this one decided there was stop codons so we need to get rid of it 
comb_germline <- filter(comb_germline, clone_id != "201657034_5419_9795")

# now you're ready to formatClones
# Try on the cluster if that doesn't work change minseq=2 and dup_singles=FALSE
print("formatting clones")
fl = formatClones(filter(comb_germline,!clone_id %in% exclude), majoronly=TRUE,verbose=FALSE,
                  heavy="IGH", chain="HL", cell="new_cell_id",
                  columns=c("subject","condition"), minseq=1,
                  dup_singles=TRUE, nproc = 20, 
                  germ = "germline_alignment_d_mask", seq = "sequence_masked")
                  
# get rid of that one weird clone 
# fl <- filter(fl, clone_id != c("2010113873_7608_3170", "201657034_5419_9795"))
print("formatting clones completed")


print("saving formatted clones object")
saveRDS(fl, file = "masked/formatted_clones.rds")
print("formatted clones saved")

results = list()
igphyml_exec <- "/gpfs/ysm/project/kleinstein/cj448/PhD/Kleinstein/igphyml/src/igphyml"
for(f in unique(fl$subject)){
	print(f)
	temp = filter(fl, subject == f)
    
    print(paste0("starting hl for ", f))
	hl = getTrees(temp, build="igphyml", partition="single", dir="temp_masked",id="single",
	exec=igphyml_exec, nproc=20)
    print(paste0("finished hl for ", f))
    
    print(paste0("starting hl_cf for ", f))
	hl_cf = getTrees(temp, build="igphyml", partition="cf", dir="temp_masked",id="cf",
		exec=igphyml_exec,nproc=20)
	print(paste0("finished hl_cf for ", f))
	
	print(paste0("starting hl_hl for ", f))
	hl_hl = getTrees(temp, build="igphyml", partition="hl", dir="temp_masked",id="hl",
		exec=igphyml_exec,nproc=20)
		print(paste0("finished hl_hl for ", f))
	
	print(paste0("starting hl_hlcf for ", f))
	hl_hlcf = getTrees(temp, build="igphyml", partition="hlcf", dir="temp_masked",id="hlcf",
		exec=igphyml_exec,nproc=20)
	print(paste0("finished hl_hlcf for ", f))
	
	print("creating the results file")
	results[[f]] = list(single = hl, cf = hl_cf, hl=hl_hl, hlcf=hl_hlcf)
}

saveRDS(results, "masked/results_list.rds")

lhoods = list()
for (subjects in names(results)) {
  temp_sub <- results[[subjects]]
  lh <- tibble()
  for (model in names(temp_sub)) {
    df <- 1
    if(model == "hlcf"){
      df = 3
    }
    temp_mod <- temp_sub[[model]]
    lhood <- sum(unlist(lapply(temp_mod$parameters, function(x)x$lhood)))
    to_bind <- tibble(subject = subjects, partition = model, lhood = lhood, df = df, nclone = nrow(temp_mod))
    lh <- bind_rows(lh, to_bind)
  }
  lh$relative_lhood = lh$lhood - filter(lh, partition=="single")$lhood
  lh$pvalue = 1 - pchisq(2*lh$relative_lhood, df=lh$df)
  lhoods = bind_rows(lhoods, lh)
  
}

omegas = list()
for(n in names(results)){
	temp = results[[n]]
	lh = tibble()
	params = unlist(lapply(temp, 
		function(x)x$parameters[[1]][grepl("omega",names(x$parameters[[1]]))]))

	model = unlist(lapply(strsplit(names(params),split="\\."),function(x)x[1]))
	param = unlist(lapply(strsplit(names(params),split="_"),function(x)x[2]))
	value = params
	omegas = bind_rows(omegas,tibble(subject=n, partition=model, param=param, value=value))
}

lhoods$partition[lhoods$partition == "single"] = "Single"
lhoods$partition[lhoods$partition == "cf"] = "CDR/FWR"
lhoods$partition[lhoods$partition == "hl"] = "Heavy/Light"
lhoods$partition[lhoods$partition == "hlcf"] = "Heavy/Light+CDR/FWR"

omegas$partition[omegas$partition == "single"] = "Single"
omegas$partition[omegas$partition == "cf"] = "CDR/FWR"
omegas$partition[omegas$partition == "hl"] = "Heavy/Light"
omegas$partition[omegas$partition == "hlcf"] = "Heavy/Light+CDR/FWR"

saveRDS(lhoods, "masked/lhoods.rds")
saveRDS(omegas, "masked/omegas.rds")

pdf("lratio_tests.pdf",width=8,height=4)
ggplot(lhoods, aes(x=subject,y=relative_lhood,fill=partition,
	size=nclone))+geom_point(pch=21)+
	theme_bw()+
	theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))+
	ylab("Likelihood ratio against single partition")+
	xlab("Subject name")+
	labs(size="Clones",fill="Partition Model")

ggplot(lhoods, aes(x=subject,y=-log(pvalue),fill=partition,
	size=nclone))+geom_point(pch=21)+
	theme_bw()+
	theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))+
	geom_hline(yintercept=-log(0.05),linetype="dashed",color="red")+
	ylab("-log(p value) against single partition")+
	xlab("Subject name")+
	labs(size="Clones",fill="Partition Model")

ggplot(omegas, aes(x=subject,y=value,fill=param))+geom_point(pch=21,size=2)+
	facet_wrap(.~partition, scales="free")+
	theme_bw()+
	theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))+
	ylab("Value")+
	xlab("Subject name")+
	labs(fill="Parameter")+ylim(0,2)+
	scale_fill_brewer(palette="Paired")
dev.off()


print("IT FINSHED")
