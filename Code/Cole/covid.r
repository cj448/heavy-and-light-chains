library(alakazam)
library(dowser)
library(ggtree)
library(dplyr)
library(ape)
library(shazam)
library(scoper)
library(gridExtra)
library(devtools)
library(parallel)
library(tidyr)

# setwd
#setwd("~/project/Kleinstein")

# read in the data 
heavy <- readChangeoDb("results_heavy_new.tsv")
light <- readChangeoDb("results_light_new.tsv")
print("read in the heavy and light data")

heavy$new_cell_id = paste0(heavy$sample, "-", heavy$cell_id)
light$new_cell_id = paste0(light$sample, "-", light$cell_id)

heavy_count = table(heavy$new_cell_id)
sum(heavy_count > 1)
multi_heavy_cells = names(heavy_count[heavy_count > 1])
heavy = filter(heavy, !new_cell_id %in% multi_heavy_cells)
print("heavy and light data finished reformating")
#You'll also need to specify cell="new_cell_id" for getSubclones and formatClones to ensure unique cell ids are used.

comb = getSubclones(heavy, light, cell="new_cell_id", nproc=20)
print("comb created")

writeChangeoDb(comb, file="comb.tsv")
comb = readChangeoDb("comb.tsv")

comb$clone_id = paste0(comb$clone_id, "-", comb$subclone_id)

references = readIMGT(dir = "~/share/germlines/imgt/human/vdj")
h = createGermlines(filter(comb,locus=="IGH"),references)
k = createGermlines(filter(comb,locus=="IGK"),references,locus="IGK")
l = createGermlines(filter(comb,locus=="IGL"),references,locus="IGL")
print("h, l, k, created")
comb_germline = bind_rows(h, l, k)
comb_germline = comb_germline[which(!is.na(comb_germline$c_call)),]
print("comb_germline created")
# minseq use it
hlclones = formatClones(comb_germline,chain="HL",
                        split_light=TRUE,heavy="IGH",cell="new_cell_id",
                        trait=c("vj_gene","c_call"), minseq = 3, collapse = FALSE, nproc=20)
# filter nas here too for c_call
hclones = formatClones(comb_germline,chain="H",
                       split_light=TRUE,heavy="IGH",cell="new_cell_id",
                       trait=c("vj_gene","c_call"), minseq = 3, collapse = FALSE, nproc=20)


print(mean(hlclones$seqs == hclones$seqs))

# no require c_call or to filter out the na values in c_call 
# build with max parsimony
#hltrees = getTrees(hlclones)
#htrees = getTrees(hclones)
print("hclones and hlclones created")


# remove stuff that is no longer needed
rm(heavy)
rm(light)
rm(heavy_count)
rm(multi_heavy_cells)
rm(comb)
rm(comb_germline)
rm(references)
rm(h)
rm(k)
rm(l)
print('optimization complete')


comb_germline <- observedMutations(comb_germline, 
      sequenceColumn="sequence_alignment",
       germlineColumn="germline_alignment_d_mask",
       regionDefinition=IMGT_V,
       frequency=TRUE,
       combine=TRUE, 
       nproc=7)


# include only cells with exactly one heavy and light chain locus
light_count = table(filter(comb_germline, locus != "IGH")$new_cell_id)
sum(light_count > 1)
multi_light_cells = names(light_count[light_count > 1])
comb_germline = filter(comb_germline, !new_cell_id %in% multi_light_cells)

# table of heavy vs light mutation frequencies
heavy_light = comb_germline %>%
  group_by(new_cell_id) %>%
  select(new_cell_id, locus, mu_freq) %>%
  spread(locus, mu_freq) %>%
  filter(!is.na(IGH)) %>%
  filter(sum(c(is.na(IGK),is.na(IGL))) == 1) %>%
  mutate(Heavy = IGH, Light = sum(c(IGK,IGL),na.rm=TRUE))

max(table(heavy_light$new_cell_id))

write.csv(heavy_light, file="heavy_light.csv")
heavy_light=read.csv("heavy_light.csv")
maxshm = max(c(heavy_light$Heavy,heavy_light$Light))
pdf("heavy_light.pdf",width=5,height=4,useDingbats=FALSE)
ggplot(heavy_light, aes(x=Heavy, y=Light)) + geom_point() + 
geom_abline(color="red") + xlim(0,maxshm) + ylim(0,maxshm) +
xlab("Heavy chain mutation frequency") +
ylab("Light chain mutation frequency") + 
theme_bw()
dev.off()

# do the boostrapping 
# home made bootstrap -- requires ape and dplyr -- 
print('reading in the bootstrapping functions')
bootstrapping <- function(starting_tree_list, b_num, nproc, tree_num){
  b = bootstrapTrees(starting_tree_list, bootstraps=b_num, rm_temp=FALSE, nproc = nproc)
  tree = lapply(b$trees, function(x)x[[tree_num]])
  return(tree)
}

full_tree_func <- function(full_tree, tree_num){
  full_tree_df <- splits_func(list(full_tree),1)
  return(full_tree_df)
}

splits_func <- function(tree, b_num){
  # NOTE: ASSUMES subtrees list is indexed by internal node number (seems to be the case)
  splits <- data.frame(found=I(lapply(subtrees(tree[[b_num]]),function(x)x$tip.label)))
  splits$node <- (Ntip(tree[[b_num]]) + 1):(Ntip(tree[[b_num]]) + tree[[b_num]]$Nnode)
  
  # find the difference between tip labels and the tips in 'found'
  full_tips <- as.vector(tree[[b_num]]$tip.label)
  absent <- c()
  for(i in 1:length(splits$found)){
    found <- as.vector(splits$found[[i]])
    not_found <- setdiff(full_tips, found)
    absent[i] <- list(not_found)
  }
  splits$absent <- data.frame(absent=I(absent))
  splits$tree_num <- b_num
  # reorder it to make sense -- tree number, node number, found tips, and absent tips
  splits <- splits[, c(4,2, 1, 3)]
  return(splits)
}

bootstrap_df <- function(tree, b_num){
  splits <- c()
  for(i in 1:length(tree)){
    to_bind <- splits_func(tree,i)
    splits <- rbind(splits, to_bind)
  }
  return(splits)
}

matching_function_parallel <- function(full_tree_df, bootstrap_df, nproc){
  match_vector = unlist(mclapply(unique(full_tree_df$node), function(node){
    #print(node)
    sub_full_tree_df <- full_tree_df[which(full_tree_df$node==node),]
    matches = unlist(lapply(1:length(bootstrap_df$tree_num), function(x){
      match_test1 <- dplyr::setequal(bootstrap_df$found[[x]], sub_full_tree_df$found[[1]])
      match_test2 <- dplyr::setequal(bootstrap_df$found[[x]], sub_full_tree_df$absent[[1]])
      if(match_test1 == TRUE || match_test2 == TRUE){
        return(1)
      }else{
        return(0)
      }}))
    sum(matches)
  },mc.cores=nproc))
  match_vector <- match_vector[!is.na(match_vector)]
  return(match_vector)
}


bootstrapping_full <- function(starting_tree_list, full_tree, b_num, nproc, tree_num){
  b_tree <- bootstrapping(starting_tree_list, b_num, nproc, tree_num)
  full_tree <- full_tree_func(full_tree, tree_num)
  bootstraps_df <- bootstrap_df(b_tree, b_num)
  results <- matching_function_parallel(full_tree, bootstraps_df, nproc)
  return(results)
}
print('functions read in')
# bootstrapping_full returns the matching vector -- used for plotting 

# do bootstrapping for all clones within hlcones and hclones
bootstrap_amount <- 100
print('starting the boostrapping -- this may take awhile')
# do bootstrapping for all clones within hlcones and hclones
print(paste0('bootstrap amount is ', bootstrap_amount))
print('starting the hclones bootstrapping')
hclones_avg <- c()
pdf("plots_covid_hclones.pdf")
for (i in 1:length(hclones$clone_id)) {
  full_tree <- getTrees(hclones[i,])$trees[[1]]
  full_tree$bootstrap <- bootstrapping_full(starting_tree_list = hclones[i,], full_tree = full_tree, b_num = bootstrap_amount, nproc = 20, tree_num = 1)
  plot(full_tree)
  nodelabels(full_tree$bootstrap)
  tobind <- mean(full_tree$bootstrap)
  hclones_avg <- rbind(hclones_avg, tobind)
  print(paste0("Finished ", i, " of ", length(hclones$clone_id)))
}
dev.off()
hclones_avg <- as.data.frame(hclones_avg)
write.csv(hclones_avg, file = "covid_hclones_avg.csv")
# get rid of now unused variables 
rm(hclones)
print('hclone plots completed and average bootstrap score found')

hlclones_avg <- c()
pdf("plots_covid_hlclones.pdf")
for (i in 1:length(hlclones$clone_id)) {
  full_tree <- getTrees(hlclones[i,])$trees[[1]]
  full_tree$bootstrap <- bootstrapping_full(starting_tree_list = hlclones[i,], full_tree = full_tree, b_num = bootstrap_amount, nproc = 20, tree_num = 1)
  plot(full_tree)
  nodelabels(full_tree$bootstrap)
  tobind <- mean(full_tree$bootstrap)
  hlclones_avg <- rbind(hlclones_avg, tobind)
  print(paste0("Finished ", i, " of ", length(hlclones$clone_id)))
}
dev.off()
hlclones_avg <- as.data.frame(hlclones_avg)
write.csv(hlclones_avg, file = "hlclones_avg.csv")
# get rid of now unused variables 
rm(hlclones)
print('hlclone plots completed and average bootstrap score found')


hclones_avg <- read.csv("covid_hclones_avg.csv")
hlclones_avg <- read.csv("hlclones_avg.csv")


hclones_avg$X <- 1:length(hclones_avg[,1])
colnames(hclones_avg) <- c("tree_num", "average_hclones")
hlclones_avg$X <- 1:length(hlclones_avg[,1])
colnames(hlclones_avg) <- c("tree_num", "average_hlclones")

# make sure they're dfs 
hclones_avg <- as.data.frame(hclones_avg)
hlclones_avg <- as.data.frame(hlclones_avg)

comp <- merge(hclones_avg, hlclones_avg)

for(i in 1:length(hclones_avg$tree_num)){
  if((comp$average_hclones[i] > comp$average_hlclones[i]) == T){
    comp$larger_value[i] <- "hclones"
    comp$l_value[i] <- comp$average_hclones[i]
  }
  else if((comp$average_hlclones[i] > comp$average_hclones[i]) == T){
    comp$larger_value[i] <- "hlclones"
    comp$l_value[i] <- comp$average_hlclones[i]
  }
  else{
    comp$larger_value[i] <- "same"
    comp$l_value[i] <- comp$average_hclones[i]
  }
}


jpeg("table.jpeg", type="cairo")
p <- tableGrob(as.data.frame(table(comp$larger_value)))
grid.arrange(p)
dev.off()

library(ggplot2)
jpeg(file = "Larger of three COVID.jpeg", type="cairo", height = 1000, width = 1000)
legendtitle <- "Larger Clone Type"
avg_overall <- ggplot(comp, aes(x=tree_num, y = l_value, color = larger_value)) + geom_point(size = 5)
avg_overall + scale_color_manual(legendtitle, values = c("#627e8d", "#88afa6", "#d49421")) + ggtitle('Average Bootstrap Value by Tree') +
  xlab("Tree Number") + ylab("Average Bootstrap Value") +theme(plot.title = element_text(hjust = 0.5))
dev.off()



test <- comp[c(-3,-4,-5)]
test$type <- "hclone"
colnames(test) <- c("tree_num", "average", "type")


test1 <- comp[c(-2,-4,-5)]
test1$type <- "hlclone"
colnames(test1) <- c("tree_num", "average", "type")

test_full <- rbind(test, test1)
jpeg("hlclones_vs_hlcones COVID.jpeg", type="cairo", height = 1000, width = 1000)
legendtitle2 <- "Clone Type"
comparison <- ggplot(test_full, aes(x=tree_num, y = average, color = type)) +
  geom_point(size = 5) +
  ggtitle('Average Bootstrap Value by Tree') +
  xlab("Tree Number") + ylab("Average Bootstrap Value") +
  theme(plot.title = element_text(hjust = 0.5)) + 
  scale_color_manual(legendtitle2, values = c("#627e8d", "#88afa6")) + 
  theme(
    panel.background = element_rect(fill = "transparent"), # bg of the panel
    plot.background = element_rect(fill = "transparent", color = NA), # bg of the plot
    panel.grid.major = element_blank(), # get rid of major grid
    panel.grid.minor = element_blank(), # get rid of minor grid
    legend.background = element_rect(fill = "transparent"), # get rid of legend bg
    legend.box.background = element_rect(fill = "transparent")) # get rid of legend panel bg) #+ 
comparison
dev.off()
# get the clone sizes
comp$seq_size <- hlclones$seqs

jpeg(file="text_plot_covid.jpeg", type="cairo", height = 1000, width = 1000)
test <- ggplot(comp, aes(x=average_hclones, y = average_hlclones)) + 
  geom_text(aes(label=seq_size, size = 5)) + 
  theme_bw() +
  ggtitle('Average Bootstrap Value of Hlclones by Hclones') +
  xlab("hclones") + ylab("hlcones") +
  theme(plot.title = element_text(hjust = 0.5)) + 
  geom_abline(slope = 1) 
test
dev.off()

write.csv(comp, file = "comp_covid.csv")
