################### Publicatoin Figures #################################
##### single column width is 86 mm 
##### full column width is 178 mm

library(dplyr)
library(ggplot2)
library(dowser)
library(alakazam)
library(shazam)
library(gridExtra)
library(ggpubr)
library(ggExtra)


########################## Figure 1 ############################################

# Mu freq figures
# simulations 
get_shmu_mu_data <- function(rep){
  shmu <- readRDS(paste0("~/Downloads/comb_125_sel_", rep, ".rds"))
  shmu_h <- filter(shmu, locus == "IGH")
  shmu_l <- filter(shmu, locus != "IGH")
  heavy_shmu <- observedMutations(shmu_h, sequenceColumn="sequence_alignment",
                                  germlineColumn="germline_alignment",
                                  regionDefinition=IMGT_V,
                                  frequency=TRUE, 
                                  combine=TRUE,
                                  nproc=10)
  means <- c()
  medians <- c()
  clone_id <- c()
  for(clone in unique(heavy_shmu$clone_id)){
    sub_obs <- filter(heavy_shmu, clone_id == clone)
    means <- append(means, mean(sub_obs$mu_freq))
    medians <- append(medians, median(sub_obs$mu_freq))
    clone_id <- append(clone_id, clone)
  }
  mean_heavy_shmu <- data.frame(means_heavy = means, medians_heavy = medians, 
                                clone_id = clone_id)
  
  light_shmu <- observedMutations(shmu_l, sequenceColumn="sequence_alignment",
                                  germlineColumn="germline_alignment",
                                  regionDefinition=IMGT_V,
                                  frequency=TRUE, 
                                  combine=TRUE,
                                  nproc=10)
  means <- c()
  medians <- c()
  clone_id <- c()
  for(clone in unique(light_shmu$clone_id)){
    sub_obs <- filter(light_shmu, clone_id == clone)
    means <- append(means, mean(sub_obs$mu_freq))
    medians <- append(medians, median(sub_obs$mu_freq))
    clone_id <- append(clone_id, clone)
  }
  mean_light_shmu <- data.frame(means_light = means, medians_light = medians, 
                                clone_id = clone_id)
  
  heavy_shmu <- heavy_shmu[order(heavy_shmu$sequence_id),]
  light_shmu <- light_shmu[order(light_shmu$sequence_id),]
  mean_heavy_shmu <- mean_heavy_shmu[order(mean_heavy_shmu$clone_id),]
  mean_light_shmu <- mean_light_shmu[order(mean_light_shmu$clone_id),]
  
  sim_data <- data.frame(heavy = heavy_shmu$mu_freq, clone = heavy_shmu$clone_id)
  sim_data <- merge(sim_data, data.frame(light= light_shmu$mu_freq, clone= light_shmu$clone_id), by = "clone")
  sim_data_means <- merge(mean_heavy_shmu, mean_light_shmu, by = "clone_id")
}

shmu_data <- c()
for(i in 1:20){
  tobind <- get_shmu_mu_data(i)
  shmu_data <- rbind(shmu_data, tobind)
}


means_shmu <- ggplot(shmu_data, aes(x = means_heavy, y = means_light)) + theme_bw() + 
  geom_point(alpha = 0.5) + geom_abline(colour = "black") + xlab("Heavy Chain SHM") +
  xlim(0, 0.1209291) + 
  ylim(0, 0.1209291) +
  theme(axis.text = element_text(size=8)) +
  ylab("Light Chain SHM") + ggtitle("Simulations") +
  theme(plot.title = element_text(hjust = 0.5, size = 12, face = "bold", vjust=-1),
        axis.text = element_text(size=9),
        axis.text.x = element_text(angle = 90))

means_shmu<- ggMarginal(means_shmu)

# now do severe covid 
get_emp_mu_data <- function(disease, cell_id){
  comb <- readRDS(paste0("~/Downloads/comb_germlines_", disease, "_0_pratchet.rds"))
  hlclones <- readRDS(paste0("~/Downloads/hlclones_", disease, "_0_pratchet.rds"))
  cell_ids <- c()
  for(clone in 1:nrow(hlclones)){
    sub <- hlclones[clone,]
    cell_ids <- append(cell_ids, sub$data[[1]]@data[[cell_id]])
  }
  if(disease == "severe_covid"){
    comb <- filter(comb, new_cell_id %in% cell_ids)
  } else if(disease == "flu"){
    comb <- filter(comb, cell_id %in% cell_ids)
  }
  
  heavy <- filter(comb, locus == "IGH")
  light <- filter(comb, locus != "IGH")
  heavy <- observedMutations(heavy, sequenceColumn="sequence_alignment",
                                germlineColumn="germline_alignment_d_mask",
                                regionDefinition=IMGT_V,
                                frequency=TRUE, 
                                combine=TRUE,
                                nproc=10)
  means <- c()
  medians <- c()
  clone_id <- c()
  for(clone in unique(heavy$clone_id)){
    sub_obs <- filter(heavy, clone_id == clone)
    means <- append(means, mean(sub_obs$mu_freq))
    medians <- append(medians, median(sub_obs$mu_freq))
    clone_id <- append(clone_id, clone)
  }
  mean_heavy <- data.frame(means_heavy = means, medians_heavy = medians, clone_id = clone_id)
  
  light <- observedMutations(light, sequenceColumn="sequence_alignment",
                                germlineColumn="germline_alignment_d_mask",
                                regionDefinition=IMGT_V,
                                frequency=TRUE, 
                                combine=TRUE,
                                nproc=10)
  means <- c()
  medians <- c()
  clone_id <- c()
  for(clone in unique(light$clone_id)){
    sub_obs <- filter(light, clone_id == clone)
    means <- append(means, mean(sub_obs$mu_freq))
    medians <- append(medians, median(sub_obs$mu_freq))
    clone_id <- append(clone_id, clone)
  }
  mean_light <- data.frame(means_light = means, medians_light = medians, clone_id = clone_id)
  
  heavy <- heavy[order(heavy$sequence_id),]
  light <- light[order(light$sequence_id),]
  mean_heavy <- mean_heavy[order(mean_heavy$clone_id),]
  mean_light <- mean_light[order(mean_light$clone_id),]
  
  data <- data.frame(heavy = heavy$mu_freq, clone = heavy$clone_id)
  data <- merge(data, data.frame(light= light$mu_freq, clone= light$clone_id), by = "clone")
  data_means <- merge(mean_heavy, mean_light, by = "clone_id")
  return(data_means)
}

sc_means <- get_emp_mu_data("severe_covid", "new_cell_id")

means_sc <- ggplot(sc_means, aes(x = means_heavy, y = means_light)) + theme_bw() + 
  geom_point(colour="#808000", alpha = 0.7) + geom_abline(colour = "black") + xlab("Heavy Chain SHM") +
  xlim(0, 0.1209291) + 
  ylim(0,0.1209291) +
  ylab("Light Chain SHM") + ggtitle("SAR-CoV-2") +
  theme(plot.title = element_text(hjust = 0.5, size = 12, face = "bold", vjust=-1),
        axis.text = element_text(size=9),
        axis.text.x = element_text(angle = 90))

means_sc<- ggMarginal(means_sc, colour = "#808000")
means_sc

# Flu data 

flu_means <- get_emp_mu_data("flu", "cell_id")

means_flu <- ggplot(flu_means, aes(x = means_heavy, y = means_light)) + theme_bw() + 
  geom_point(colour = "#CC5500", alpha = 0.7) + geom_abline(colour = "black") + xlab("Heavy Chain SHM") +
  xlim(0, 0.1209291) + 
  ylim(0,0.1209291) +
  ylab("Light Chain SHM") + ggtitle("Influenza") +
  theme(plot.title = element_text(hjust = 0.5, size = 12, face = "bold", vjust=-1),
        axis.text = element_text(size=9),
        axis.text.x = element_text(angle = 90))

means_flu<- ggMarginal(means_flu, colour = "#CC5500")
means_flu

pdf(file = "~/Documents/figures/fig_1.pdf", width = 7.00787,
    height = 3)
grid.arrange(means_sc, means_flu, means_shmu, ncol=3)
dev.off()


########################## Figure 2 ############################################


# make the RF data into a boxplot friendly format 
make_base_rf_graph <- function(data, title){
  mean_results <- readRDS(paste0("~/Downloads/full_results_rf", data, "_all_iter_.rds"))
  mean_results <- filter(mean_results, prop_masked == 0.00)
  temp <- mean_results[, c(1:2, 4)]
  temp2 <- mean_results[, c(1,3, 4)]
  temp2$status <- "Heavy and Light"
  temp$status <- "Heavy"
  colnames(temp2) <- c("clone_id", "distance", "seqs", "status")
  colnames(temp) <- c("clone_id", "distance", "seqs", "status")
  temp <- rbind(temp, temp2)
  rm(temp2)
  
  graph <- ggpaired(temp, x='status', y = 'distance', fill = 'status',
                    line.color = 'gray', line.size = 0.4, palette =  c("#0080C6", "#FFC20E"),
                    title = title) +
    theme_bw() +
    #xlim(, (max(c(sim_data_flu$heavy,sim_data_flu$light)))) + 
    ylim(4, 60) +
    xlab("Partition") + ylab("RF Distance") +
    labs(size = "Clone Size") +
    stat_compare_means(method="wilcox", comparisons = list(c("Heavy", "Heavy and Light")),
                       size=3,paired=TRUE, label.y = 56) +
    theme(legend.position = "none") +
    theme(plot.title = element_text(hjust = 0.5, size = 12, face = "bold"),
          axis.text = element_text(size=12)) +
    scale_x_discrete(labels = c("H", "H+L"))
  return(graph)
}

pratchet_rf <- make_base_rf_graph("pratchet", "Phangorn Parsimony")
pml_rf <- make_base_rf_graph("pml", "Phangorn ML")
dnapars_rf <- make_base_rf_graph("dnapars", "PHYLIP Parsimony")
dnaml_rf <- make_base_rf_graph("dnaml", "PHYLIP ML")
igphyml_rf <- make_base_rf_graph("igphyml", "IgPhyML")
igphyml_p_rf <- make_base_rf_graph("igphyml_p", "IgPhyML Part")
raxml_rf <- make_base_rf_graph("raxml", "RAxML")
raxml_p_rf <- make_base_rf_graph("raxml_p", "RAxML Part")


# find the mean rf by build method
mean_rf_build_func <- function(title){
  build <- c("pratchet", "pml", "dnapars", "dnaml", "igphyml", "igphyml_p", "raxml", "raxml_p")
  rf_mean <- c()
  for(i in unique(build)){
    mean_results <- readRDS(paste0("~/Downloads/full_results_rf", i, "_all_iter_.rds"))
    mean_results <- filter(mean_results, prop_masked == 0.00)
    temp <- mean_results[, c(1:2, 4)]
    temp2 <- mean_results[, c(1,3, 4)]
    temp2$status <- "Heavy and Light"
    temp$status <- "Heavy"
    colnames(temp2) <- c("clone_id", "distance", "seqs", "status")
    colnames(temp) <- c("clone_id", "distance", "seqs", "status")
    temp <- rbind(temp, temp2)
    rm(temp2)
    tobind <- data.frame(distance = c(mean(filter(temp, status == "Heavy")$distance), 
                         mean(filter(temp, status == "Heavy and Light")$distance)),
                         status = c("Heavy", "Heavy and Light"),
                         method = c(i, i))
    rf_mean <- rbind(rf_mean, tobind)
  }
  rf_mean$method <- c(rep("Phangorn Parsimony",2), rep("Phangorn ML", 2), rep("PHYLIP Parsimony",2),
                      rep("PHYLIP ML",2), rep("IgPhyML", 2), rep("IgPhyML Part", 2), 
                      rep("RAxML", 2), rep("RAxML Part", 2))
  rf_mean <- rf_mean[order(rf_mean$status),]
  
  means_graph <- ggpaired(rf_mean, x='status', y = 'distance', fill = 'status',
                          line.color = 'gray', line.size = 0.4,
                          title = title) +
    scale_fill_manual(values = c("#0080C6", "#FFC20E")) +
    theme_bw() +
    ylim(4, 60) +
    xlab("Partition") + ylab("") +
    labs(size = "Clone Size") +
    stat_compare_means(method="wilcox", comparisons = list(c("Heavy", "Heavy and Light")),
                       size=3,paired=TRUE, label.y = 56) +
    #theme(legend.position = "none") +
    theme(plot.title = element_text(hjust = 0.5, size = 12, face = "bold"),
          axis.text = element_text(size=12)) +
    geom_point(aes(color=as.factor(method)))+
    scale_color_brewer(palette = "Dark2") +
    scale_x_discrete(labels = c("H", "H+L"))
  return(means_graph)
}


rf_mean <- mean_rf_build_func("Mean")

# update for the dimension 
pdf(file = "~/Documents/figures/rf_plots_all_build.pdf", width = 8.759837,
    height = 5)
grid.arrange(pratchet_rf, pml_rf, dnapars_rf, dnaml_rf, igphyml_rf, 
             igphyml_p_rf, raxml_rf, raxml_p_rf, ncol=4)
dev.off()

pdf(file = "~/Documents/figures/fig_2_a.pdf", width = 7.00787,
    height = 3)
grid.arrange(pratchet_rf, igphyml_rf, igphyml_p_rf, rf_mean, ncol=4)
dev.off()

# get the legend figure 
pdf(file = "~/Documents/figures/fig_2_legend.pdf", width = (7.00787/2),
    height = 3)
grid.arrange(rf_mean, ncol=1)
dev.off()

##### now the sim BS image
sim_pratchet <- read.csv(".../.../comp_0_pratchet_125_nosel.csv")
sim_pml <- read.csv(".../.../comp_0_pml_125_nosel.csv")
sim_dnaml <- read.csv(".../.../comp_0_dnaml_125_nosel.csv")
sim_dnapars <- read.csv(".../.../comp_0_dnapars_125_nosel.csv")

profile <- function(df, title){
  heavy <- as.data.frame(rep("heavy", length(df$tree_num)))
  hl <- as.data.frame(rep("heavy_light", length(df$tree_num)))
  colnames(heavy) <- c("group")
  colnames(hl) <- c("group")
  my_data <- rbind(heavy, hl)
  hbs_scores <- as.data.frame(df$hclones_avg)
  hlbs_scores <- as.data.frame(df$hlclones_avg)
  colnames(hbs_scores) <- c("score")
  colnames(hlbs_scores) <- c("score")
  bob <- rbind(hbs_scores, hlbs_scores)
  my_data <- cbind(my_data, bob)#, total_branch_length,avg_branch_length)
  cole_palette <- c("#0080C6", "#FFC20E")
  profile <- ggpaired(my_data, x = "group", y = "score",
                      fill = "group", line.color = "gray", line.size = 0.4,
                      palette = cole_palette, title = title) +
    theme_bw() +
    xlab("Partition") +
    ylab("Bootstrap Score") +
    ylim(35, 98) +
    theme(plot.title = element_text(hjust = 0.5, size = 12, face = "bold"),
          axis.text = element_text(size=12))+
    theme(legend.position = "none") +
    stat_compare_means(method = "wilcox", size=3, paired=TRUE, comparisons = list(c("heavy", "heavy_light")),
                       label.y = 88)+
    scale_x_discrete(labels = c("H", "H+L"))
  return(profile)
}

pratchet <- profile(sim_pratchet, "Parsimony")
pml <- profile(sim_pml, "ML")
dnaml <- profile(sim_dnaml, "DNAML")
dnapars <- profile(sim_dnapars, "DNAPARS")

# make mean BS by type 
sim_bs_means <- data.frame(distance = c(mean(sim_pratchet$hclones_avg), mean(sim_pratchet$hlclones_avg), 
                                   mean(sim_pml$hclones_avg), mean(sim_pml$hlclones_avg),
                                   mean(sim_dnaml$hclones_avg), mean(sim_dnaml$hlclones_avg),
                                   mean(sim_dnapars$hclones_avg), mean(sim_dnapars$hlclones_avg)),
                      status = c("Heavy", "Heavy and Light", "Heavy", "Heavy and Light","Heavy", "Heavy and Light","Heavy", "Heavy and Light"),
                      method = c("Phangorn_Parsimony", "Phangorn_Parsimony", "Phangorn_ML", "Phangorn_ML", "DNAML", "DNAML", "DNAPARS", "DNAPARS"))
sim_bs_means <- sim_bs_means[order(sim_bs_means$status),]

sim_means_graph <- ggpaired(sim_bs_means, x='status', y = 'distance', fill = 'status',
                        line.color = 'gray', line.size = 0.4,
                        title = "Simulation Mean") +
  scale_fill_manual(values = c("#0080C6", "#FFC20E")) +
  theme_bw() +
  theme(legend.position = "none") +
  ylim(35, 98) +
  xlab("Partition") + ylab("Bootstrap Score") +
  labs(size = "Clone Size") +
  stat_compare_means(method="wilcox", comparisons = list(c("Heavy", "Heavy and Light")),
                     size=3,paired=TRUE, label.y = 88) +
  theme(plot.title = element_text(hjust = 0.5, size = 12, face = "bold"),
        axis.text = element_text(size=12)) +
  geom_point(aes(color=as.factor(method)))+
  scale_color_brewer(palette = "Dark2") +
  scale_x_discrete(labels = c("H", "H+L"))

#pdf(file = "bs_sim_plots.pdf", width = 3.389444444,
#    height = 3)
#sim_means_graph
#dev.off()

### now do the empircal data 
flu_pratchet <- read.csv(".../.../comp_flu_0_pratchet_masked.csv")
flu_pml <- read.csv(".../.../comp_flu_0_pml_masked.csv")
flu_dnaml <- read.csv(".../.../comp_flu_0_dnaml_masked.csv")
flu_dnapars <- read.csv(".../.../comp_flu_0_dnapars_masked.csv")

flu_pratchet_graph <- profile(flu_pratchet, "Parsimony")
flu_pml_graph <- profile(flu_pml, "ML")
flu_dnaml_graph <- profile(flu_dnaml, "DNAML")
flu_dnapars_graph <- profile(flu_dnapars, "DNAPARS")

flu_bs_means <- data.frame(distance = c(mean(flu_pratchet$hclones_avg), mean(flu_pratchet$hlclones_avg), 
                                        mean(flu_pml$hclones_avg), mean(flu_pml$hlclones_avg),
                                        mean(flu_dnaml$hclones_avg), mean(flu_dnaml$hlclones_avg),
                                        mean(flu_dnapars$hclones_avg), mean(flu_dnapars$hlclones_avg)),
                           status = c("Heavy", "Heavy and Light", "Heavy", "Heavy and Light","Heavy", "Heavy and Light","Heavy", "Heavy and Light"),
                           method = c("Phangorn_Parfluony", "Phangorn_Parfluony", "Phangorn_ML", "Phangorn_ML", "DNAML", "DNAML", "DNAPARS", "DNAPARS"))
flu_bs_means <- flu_bs_means[order(flu_bs_means$status),]

flu_means_graph <- ggpaired(flu_bs_means, x='status', y = 'distance', fill = 'status',
                            line.color = 'gray', line.size = 0.4,
                            title = "Influenza Mean") +
  scale_fill_manual(values = c("#0080C6", "#FFC20E")) +
  # scale_color_manual(values = c("#0080C6", "#FFC20E", "red", "green")) +
  theme_bw() +
  #xlim(, (max(c(sim_data_flu$heavy,sim_data_flu$light)))) + 
  ylim(35, 98) +
  xlab("Partition") + ylab("Bootstrap Score") +
  labs(size = "Clone Size") +
  stat_compare_means(method="wilcox", comparisons = list(c("Heavy", "Heavy and Light")),
                     size=3,paired=TRUE, label.y = 88) +
  theme(legend.position = "none") +
  theme(plot.title = element_text(hjust = 0.5, size = 12, face = "bold"),
        axis.text = element_text(size=12)) +
  geom_point(aes(color=as.factor(method)))+
  scale_color_brewer(palette = "Dark2") +
  scale_x_discrete(labels = c("H", "H+L"))


pdf(file = ".../.../bs_plots.pdf", width = 8.759837,
    height = 4.5)
grid.arrange(pratchet, pml, dnaml, dnapars, sim_means_graph,
             flu_pratchet_graph, flu_pml_graph, flu_dnaml_graph, flu_dnapars_graph,
             flu_means_graph, ncol=5)
dev.off()

# remove the legned and save this 
pdf(file=".../.../flu_bs_plot.pdf", width = 3.389444444,
    height = 3)
flu_means_graph
dev.off()

#### now do the last figure 
## BS first 
avg_pratchet <- read.csv(".../.../avg_df_pratchet_125_nosel.csv")
avg_pml <- read.csv(".../.../avg_df_pml_125_nosel.csv")
avg_dnaml <- read.csv(".../.../avg_df_dnaml_125_nosel.csv")
avg_dnapars <- read.csv(".../.../avg_df_dnapars_125_nosel.csv")

bs_all <- rbind(avg_dnaml,avg_dnapars, avg_pml, avg_pratchet)

bs <- ggplot(bs_all, aes(x=prop, y=values))+
  ggtitle("Average Bootstrap Score by Light Chain Inculsion Percentage")+
  theme_bw() +
  geom_line(aes(color = build), size = 2, alpha = 0.8) +
  geom_point(aes(color=build), size = 5, alpha = 0.75) +
  theme(plot.title = element_text(hjust = 0.5, size = 12, face = "bold"),
        axis.text = element_text(size=9)) +
  scale_color_brewer(palette = "Dark2") +
  ylab("Bootstrap Score") +
  xlab("Amount of Light Chain Included")
bs$labels$colour <- "Method"
bs

# now the rf one 
# the rf input here was generated using this script, but looped through with the
# different tree builds -- here is pml for example
build <- "pml"
data_source <- "125_nosel"

comp_0 <- readRDS(paste0(".../.../boxplots_data_0_", build, "_", data_source, ".rds"))
comp_0 <- comp_0[complete.cases(comp_0),]
comp_0$prop <- 0
comp_5 <- readRDS(paste0(".../.../boxplots_data_0.05_", build, "_", data_source, ".rds"))
comp_5 <- comp_5[complete.cases(comp_5),]
comp_0$prop <- 5
comp_10 <- readRDS(paste0(".../.../boxplots_data_0.1_", build, "_", data_source, ".rds"))
comp_10 <- comp_10[complete.cases(comp_10),]
comp_10$prop <- 10
comp_15 <- readRDS(paste0(".../.../boxplots_data_0.15_", build, "_", data_source, ".rds"))
comp_15 <- comp_15[complete.cases(comp_15),]
comp_15$prop <- 15
comp_20 <- readRDS(paste0(".../.../boxplots_data_0.2_", build, "_", data_source, ".rds"))
comp_20 <- comp_20[complete.cases(comp_20),]
comp_20$prop <- 20
comp_30 <- readRDS(paste0(".../.../boxplots_data_0.3_", build, "_", data_source, ".rds"))
comp_30 <- comp_30[complete.cases(comp_30),]
comp_30$prop <- 30
comp_40 <- readRDS(paste0(".../.../boxplots_data_0.4_", build, "_", data_source, ".rds"))
comp_40 <- comp_40[complete.cases(comp_40),]
comp_40$prop <- 40
comp_50 <- readRDS(paste0(".../.../boxplots_data_0.5_", build, "_", data_source, ".rds"))
comp_50 <- comp_50[complete.cases(comp_50),]
comp_50$prop <- 50
comp_60 <- readRDS(paste0(".../.../boxplots_data_0.6_", build, "_", data_source, ".rds"))
comp_60 <- comp_60[complete.cases(comp_60),]
comp_60$prop <- 60
comp_70 <- readRDS(paste0(".../.../boxplots_data_0.7_", build, "_", data_source, ".rds"))
comp_70 <- comp_70[complete.cases(comp_70),]
comp_70$prop <- 70
comp_80 <- readRDS(paste0("boxplots_data_0.8_", build, "_", data_source, ".rds"))
comp_80 <- comp_80[complete.cases(comp_80),]
comp_80$prop <- 80
comp_85 <- readRDS(paste0("boxplots_data_0.85_", build, "_", data_source, ".rds"))
comp_85 <- comp_85[complete.cases(comp_85),]
comp_85$prop <- 85
comp_90 <- readRDS(paste0("boxplots_data_0.9_", build, "_", data_source, ".rds"))
comp_90 <- comp_90[complete.cases(comp_90),]
comp_90$prop <- 90
comp_95 <- readRDS(paste0("boxplots_data_0.95_", build, "_", data_source, ".rds"))
comp_95 <- comp_95[complete.cases(comp_95),]
comp_95$prop <- 95
print("data read in")

hclones_avg <- mean(c(mean(filter(comp_0, status == "Heavy")$distance), mean(filter(comp_5, status == "Heavy")$distance), mean(filter(comp_15, status == "Heavy")$distance),
                      mean(filter(comp_20, status == "Heavy")$distance), mean(filter(comp_30, status == "Heavy")$distance), mean(filter(comp_40, status == "Heavy")$distance), 
                      mean(filter(comp_50, status == "Heavy")$distance), mean(filter(comp_60, status == "Heavy")$distance), mean(filter(comp_70, status == "Heavy")$distance), 
                      mean(filter(comp_80, status == "Heavy")$distance), mean(filter(comp_85, status == "Heavy")$distance), mean(filter(comp_90, status == "Heavy")$distance), 
                      mean(filter(comp_95, status == "Heavy")$distance), mean(filter(comp_10, status == "Heavy")$distance)))
filter_da_comps <- function(df){
  df <- filter(df, status == "Heavy and Light")
  return(df)
}
comp_0 <- filter_da_comps(comp_0)
comp_5 <- filter_da_comps(comp_5)
comp_10 <- filter_da_comps(comp_10)
comp_15 <- filter_da_comps(comp_15)
comp_20 <- filter_da_comps(comp_20)
comp_30 <- filter_da_comps(comp_30)
comp_40 <- filter_da_comps(comp_40)
comp_50 <- filter_da_comps(comp_50)
comp_60 <- filter_da_comps(comp_60)
comp_70 <- filter_da_comps(comp_70)
comp_80 <- filter_da_comps(comp_80)
comp_85 <- filter_da_comps(comp_85)
comp_90 <- filter_da_comps(comp_90)
comp_95 <- filter_da_comps(comp_95)

print("data filtered")

avg_hlclones <- c(mean(comp_0$distance), mean(comp_5$distance), mean(comp_10$distance), mean(comp_15$distance), mean(comp_20$distance), mean(comp_30$distance), mean(comp_40$distance), mean(comp_50$distance),
                  mean(comp_60$distance), mean(comp_70$distance), mean(comp_80$distance), mean(comp_85$distance), mean(comp_90$distance), mean(comp_95$distance))
sd <- c(sd(comp_0$distance), sd(comp_5$distance), sd(comp_10$distance), sd(comp_15$distance), sd(comp_20$distance), sd(comp_30$distance), sd(comp_40$distance), sd(comp_50$distance),
        sd(comp_60$distance), sd(comp_70$distance), sd(comp_80$distance), sd(comp_85$distance), sd(comp_90$distance), sd(comp_95$distance))
props <- c(0, 5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 85, 90, 95)
df <- data.frame(distances = avg_hlclones, props = props, sd = sd, build = build)
df <- rbind(df, data.frame(distances = hclones_avg, props = 0, sd = sd, build = build))
df <- df[1:15,]
df$distances[15] <- hclones_avg

all_builds_pratchet <- read.csv("all_builds_pratchet_125_nosel.csv")
all_builds_pratchet <- all_builds_pratchet[1:15,2:5]
all_builds_pratchet$props <- (all_builds_pratchet$props-100)*-1
all_builds_pml <- df
all_builds_dnaml <- read.csv("all_builds_dnaml_125_nosel.csv")
all_builds_dnaml <- all_builds_dnaml[1:15,2:5]
all_builds_dnaml$props <- (all_builds_dnaml$props-100)*-1
all_builds_dnapars <- read.csv("all_builds_dnapars_125_nosel.csv")
all_builds_dnapars <- all_builds_dnapars[1:15,2:5]
all_builds_dnapars$props <- (all_builds_dnapars$props-100)*-1
rf_all <- rbind(all_builds_dnaml,all_builds_dnapars, all_builds_pml, all_builds_pratchet)

rf <- ggplot(rf_all, aes(x=props, y=distances))+
  ggtitle("Average RF Distance by Light Chain Inculsion Percentage")+
  theme_bw() +
  geom_line(aes(color = build), size = 2, alpha = 0.8) +
  geom_point(aes(color=build), size = 7, alpha = 0.75) +
  theme(plot.title = element_text(hjust = 0.5, size = 12, face = "bold"),
        axis.text = element_text(size=9)) +
  scale_color_brewer(palette = "Dark2") +
  ylab("RF Distance") +
  xlab("Percent of Cells with Light Chain Missing")
rf$labels$colour <- "Method"
rf

pdf(file = "inclusion_plots.pdf", width = 7.00787,
    height = 2.5)
grid.arrange(rf, ncol=1)
dev.off()
